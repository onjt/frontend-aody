import JobZImage from "../../../common/jobz-img";
import { NavLink, useLocation } from "react-router-dom";
import { loadScript, setMenuActive } from "../../../../globals/constants";
import { admin, adminRoute, employer, empRoute, publicUser } from "../../../../globals/route-names";
import { useEffect } from "react";

function AdminSidebarSection(props) {

    const currentpath = useLocation().pathname;

    useEffect(() => {
        loadScript("js/custom.js");
        loadScript("js/emp-sidebar.js");
    })

    return (
        <>
            <nav id="sidebar-admin-wraper" className={props.sidebarActive ? "" : "active"}>
                <div className="page-logo">
                    <NavLink to={publicUser.HOME3}><JobZImage id="skin_page_logo" src="images/aody1.png" alt="aody" /></NavLink>
                </div>
                <div className="admin-nav scrollbar-macosx">
                    <ul>
                        <li
                            className={setMenuActive(currentpath, empRoute(employer.DASHBOARD))}>
                            <NavLink to={adminRoute(admin.DASHBOARD)}><i className="fa fa-home" /><span className="admin-nav-text">Tableau de bord</span></NavLink>
                        </li>

                        <li
                            className={
                                setMenuActive(currentpath, empRoute(employer.POST_A_JOB)) +
                                setMenuActive(currentpath, empRoute(employer.MANAGE_JOBS))
                            }>
                            <a href="#">
                                <i className="fa fa-user-friends" />
                                <span className="admin-nav-text">Gestion des comptes</span>
                            </a>
                            <ul className="sub-menu">
                                <li> <NavLink to={adminRoute(admin.MANAGE_STUDENT)} id="jobMenuId1"><span className="admin-nav-text">Etudiant</span></NavLink></li>
                                <li> <NavLink to={adminRoute(admin.MANAGE_COMPANY)} id="jobMenuId2"><span className="admin-nav-text">Entreprise</span></NavLink></li>
                            </ul>
                        </li>

                        <li className={setMenuActive(currentpath, empRoute(employer.CANDIDATES))}>
                            <NavLink to={adminRoute(admin.MANAGE_JOBS)}><i className="fa fa-suitcase" /><span className="admin-nav-text">Gestion des offres</span></NavLink>
                        </li>

                        <li className={setMenuActive(currentpath, empRoute(employer.PACKAGES))}>
                            <NavLink to={adminRoute(admin.SUBSCRIPTION)}><i className="fa fa-money-bill-alt" /><span className="admin-nav-text">Suivi des abonnements</span></NavLink>
                        </li>

                        <li className={setMenuActive(currentpath, empRoute(employer.RESUME_ALERTS))}>
                            <NavLink to={adminRoute(admin.MANAGE_PASSWORD)}><i class="fa fa-key" /><span className="admin-nav-text">Changement de mot de passe</span></NavLink>
                        </li>

                        <li>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#logout-dash-profile">
                                <i className="fa fa-share-square" />
                                <span className="admin-nav-text">Déconnexion</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}
export default AdminSidebarSection