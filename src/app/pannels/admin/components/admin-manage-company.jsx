import { useEffect, useState } from "react";
import axios from "axios"
import JobZImage from "../../../common/jobz-img";
import CompanyViewPopup from "../../../common/popups/popup-company-view";

function AdminManageCompany() {

    const [companyData, setCompanyData] = useState([])
    const [selectedCompany, setSelectedCompany] = useState({})

    const loadcompanyData = async () => {
        const result = await axios.get("http://localhost:8080/company/list")
        setCompanyData(result.data)
    }

    useEffect(() => {
        loadcompanyData()
    }, [])


    return (
        <>
            <div>
                <div className="wt-admin-right-page-header clearfix">
                    <h2>Entreprises</h2>
                    <div className="breadcrumbs"><a href="#">Accueil</a><a href="#">Tableau de bord</a><span>Gestion des comptes</span></div>
                </div>
                <div className="twm-pro-view-chart-wrap">
                    <div className="col-lg-12 col-md-12 mb-4">
                        <div className="panel panel-default site-bg-white m-t30">
                            <div className="panel-heading wt-panel-heading p-a20">
                                <h4 className="panel-tittle m-a0"><i className="far fa-bookmark" />Tous les entreprises</h4>
                            </div>
                            <div className="panel-body wt-panel-body">
                                <div className="twm-D_table p-a20 table-responsive">

                                    {/* id="jobs_bookmark_table" */}
                                    <table className="table table-bordered twm-bookmark-list-wrap">
                                        <thead>
                                            <tr>
                                                <th>Informations</th>
                                                <th>Site web</th>
                                                <th>Date et heure d’inscription</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                companyData.map((company, index) => (
                                                    <tr key={index}>
                                                        <td>
                                                            <div className="twm-bookmark-list">
                                                                <div className="twm-media">
                                                                    <div className="twm-media-pic">
                                                                        <JobZImage src="images/jobs-company/pic4.jpg" alt="#" />
                                                                    </div>
                                                                </div>
                                                                <div className="twm-mid-content">
                                                                    <a href="#" className="twm-job-title">
                                                                        <h4>{company.name}</h4>
                                                                    </a>
                                                                    <p className="twm-bookmark-address">
                                                                        <i className="feather-mail" />{company.email}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href={"https://" + company.website} target="_blank">{company.website}</a></td>

                                                        <td>{company.creationDate}</td>
                                                        <td><span className="text-clr-green2">{company.statusAccount}</span></td>
                                                        <td>
                                                            <div className="twm-table-controls">
                                                                <ul className="twm-DT-controls-icon list-unstyled">
                                                                    <li>
                                                                        <a onClick={() => setSelectedCompany(company)} data-bs-toggle="modal" href="#company-view" role="button" className="custom-toltip">
                                                                            <span className="fa fa-eye" />
                                                                            <span className="custom-toltip-block">Veiw</span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <button title="Send message" data-bs-toggle="tooltip" data-bs-placement="top">
                                                                            <span className="far fa-envelope-open" />
                                                                        </button>
                                                                    </li>
                                                                    <li>
                                                                        <button title="Delete" data-bs-toggle="tooltip" data-bs-placement="top">
                                                                            <span className="far fa-trash-alt" />
                                                                        </button>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Informations</th>
                                                <th>Site web</th>
                                                <th>Date et heure d’inscription</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <CompanyViewPopup selectedCompany={selectedCompany} />
        </>

    )
}
export default AdminManageCompany