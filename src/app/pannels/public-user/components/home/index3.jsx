import { NavLink } from "react-router-dom";
import { loadScript, publicUrlFor, default_skin, updateSkinStyle } from "../../../../../globals/constants";
import { publicUser } from "../../../../../globals/route-names";
import JobZImage from "../../../../common/jobz-img";
import CountUp from "react-countup";
import { useEffect } from "react";

function Home3Page() {

    useEffect(()=>{
        updateSkinStyle(default_skin, true, false)
        loadScript("js/custom.js")
    })

    return (
        <>
            {/*Banner Start*/}
            <div className="twm-home3-banner-section site-bg-white bg-cover" style={{ backgroundImage: `url(${publicUrlFor("images/home-3/banneraody.jpg")})` }}>
                <div className="twm-home3-inner-section">
                    <div className="twm-bnr-mid-section">
                        <div className="twm-bnr-title-large">Découvrez avec nous</div>
                        <div className="twm-bnr-discription">Trouvez votre prochaine expérience dans le monde professionnelle.</div>
                    </div>
                    <div className="twm-bnr-bottom-section">
                        
                        <div className="twm-bnr-blocks-wrap">
                            {/*icon-block-1*/}
                            <div className="twm-bnr-blocks twm-bnr-blocks-position-1">
                                <div className="twm-content">
                                    <div className="tw-count-number text-clr-pink">
                                        <span className="counter">
                                            <CountUp end={20} duration={10} />
                                        </span>+
                                    </div>
                                    <p className="icon-content-info">Entreprises</p>
                                </div>
                            </div>
                            
                            {/*icon-block-3*/}
                            <div className="twm-bnr-blocks-3 twm-bnr-blocks-position-3">
                                
                                <div className="twm-content">
                                    <div className="tw-count-number text-clr-green">
                                        <span className="counter">
                                            <CountUp end={100} duration={10} />
                                            </span>+
                                    </div>
                                    <p className="icon-content-info">Recrutements</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*Banner End*/}
            {/* TOP COMPANIES START */}
            <div className="section-full p-t120 p-b90 site-bg-white twm-companies-wrap">
                {/* title="" START*/}
                <div className="section-head center wt-small-separator-outer">
                    <div className="wt-small-separator site-text-primary">
                        <div>Top Entreprises</div>
                    </div>
                    <h2 className="wt-title">Candidater auprès d'eux</h2>
                </div>
                {/* title="" END*/}
                <div className="container">
                    <div className="section-content">
                        <div className="owl-carousel home-client-carousel3 owl-btn-vertical-center">
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w1.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w2.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w3.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w4.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w5.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w6.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w1.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w2.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w3.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="ow-client-logo">
                                    <div className="client-logo client-logo-media">
                                        <NavLink to={publicUser.employer.LIST}><JobZImage src="images/client-logo2/w5.png" alt="" /></NavLink></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="twm-company-approch2-outer">
                        <div className="twm-company-approch2">
                            <div className="row">
                                {/*block 1*/}
                                <div className="col-lg-4 col-md-4">
                                    <div className="counter-outer-two">
                                        <div className="icon-content">
                                            <div className="tw-count-number site-text-black">
                                                <span className="counter">
                                                    <CountUp end={100} duration={10} />
                                                    </span>+</div>
                                            <p className="icon-content-info">Embauches</p>
                                        </div>
                                    </div>
                                </div>
                                {/*block 2*/}
                                <div className="col-lg-4 col-md-4">
                                    <div className="counter-outer-two">
                                        <div className="icon-content">
                                            <div className="tw-count-number site-text-black">
                                                <span className="counter">
                                                    <CountUp end={150} duration={10} />
                                                    </span>+</div>
                                            <p className="icon-content-info">Offres d'emploi</p>
                                        </div>
                                    </div>
                                </div>
                                {/*block 3*/}
                                <div className="col-lg-4 col-md-4">
                                    <div className="counter-outer-two">
                                        <div className="icon-content">
                                            <div className="tw-count-number site-text-black">
                                                <span className="counter">
<CountUp end={150} duration={10} />
</span>+</div>
                                            <p className="icon-content-info">Utilisateurs</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* TOP COMPANIES END */}
            
            {/* HOW IT WORK SECTION START */}
            <div className="section-full p-t120 p-b90 site-bg-yellow twm-how-it-work-area">
                <div className="container">
                    {/* title="" START*/}
                    <div className="section-head center wt-small-separator-outer">
                        <div className="wt-small-separator site-text-primary">
                            <div>Utilisation</div>
                        </div>
                        <h2 className="wt-title">Comment ça marche?</h2>
                    </div>
                    {/* title="" END*/}
                    <div className="twm-how-it-work-section3">
                        <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-6">
                                <div className="twm-w-process-steps3">
                                    <div className="twm-w-pro-top">
                                        <span className="twm-large-number  text-clr-sky">01</span>
                                        <div className="twm-media bg-clr-sky">
                                            <span><JobZImage src="images/work-process/icon1.png" alt="icon1" /></span>
                                        </div>
                                    </div>
                                    <h4 className="twm-title">Créer un compte</h4>
                                    <p>Créez un compte pour trouver un emploi dans la disponibilité qui vous convient.</p>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6">
                                <div className="twm-w-process-steps3">
                                    <div className="twm-w-pro-top">
                                        <span className="twm-large-number text-clr-pink">02</span>
                                        <div className="twm-media bg-clr-pink">
                                            <span><JobZImage src="images/work-process/icon2.png" alt="icon1" /></span>
                                        </div>
                                    </div>
                                    <h4 className="twm-title">Postuler</h4>
                                    <p>Une fois connecté(e), vous pouvez rechercher et postulez aux offres.</p>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6">
                                <div className="twm-w-process-steps3">
                                    <div className="twm-w-pro-top">
                                        <span className="twm-large-number  text-clr-green">03</span>
                                        <div className="twm-media  bg-clr-green">
                                            <span><JobZImage src="images/work-process/icon3.png" alt="icon1" /></span>
                                        </div>
                                    </div>
                                    <h4 className="twm-title">Mettre à jour le profil</h4>
                                    <p>Ajoutez le plus d'informations possible dans votre profil et attirez les recruteurs.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* HOW IT WORK SECTION END */}
            
            {/* JOB POST START */}
            <div className="section-full p-t120 p-b90 site-bg-gray twm-bg-ring-wrap2">
                <div className="twm-bg-ring-right" />
                <div className="twm-bg-ring-left" />
                <div className="container">
                    <div className="wt-separator-two-part">
                        <div className="row wt-separator-two-part-row">
                            <div className="col-xl-6 col-lg-6 col-md-12 wt-separator-two-part-left">
                                {/* title="" START*/}
                                <div className="section-head left wt-small-separator-outer">
                                    <div className="wt-small-separator site-text-primary">
                                        <div>Les offres d'emploi</div>
                                    </div>
                                    <h2 className="wt-title">Boostez votre carrière Vous le méritez</h2>
                                </div>
                                {/* title="" END*/}
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 wt-separator-two-part-right text-right">
                                <NavLink to={publicUser.jobs.LIST} className=" site-button">Toutes les offres</NavLink>
                            </div>
                        </div>
                    </div>
                    <div className="section-content">
                        <div className="twm-jobs-grid-wrap">
                            <div className="row">
                                <div className="col-lg-6 col-md-6">
                                    <div className="twm-jobs-grid-style1  m-b30">
                                        <div className="twm-media">
                                            <JobZImage src="images/jobs-company/pic1.jpg" alt="#" />
                                        </div>
                                        <span className="twm-job-post-duration">Hier</span>
                                        <div className="twm-jobs-category green"><span className="twm-bg-golden">Nouveau</span></div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-job-title">
                                                <h4>Web Designer , Developer</h4>
                                            </NavLink>
                                            <p className="twm-job-address">Andraharo, Antananarivo</p>
                                            <a href="https://themeforest.net/user/thewebmax/portfolio" className="twm-job-websites site-text-primary">https://thewebmax.com</a>
                                        </div>
                                        <div className="twm-right-content">
                                            <div className="twm-jobs-amount">$2000 - $2500 <span>/ Mois</span></div>
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-jobs-browse site-text-primary">Détail</NavLink>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-6">
                                    <div className="twm-jobs-grid-style1 m-b30">
                                        <div className="twm-media">
                                            <JobZImage src="images/jobs-company/pic2.jpg" alt="#" />
                                        </div>
                                        <span className="twm-job-post-duration">Il y a 15 jours</span>
                                        <div className="twm-jobs-category green"><span className="twm-bg-yellow">Job-Etudiant</span></div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-job-title">
                                                <h4>Commercial/Contracting groupe hôtélier</h4>
                                            </NavLink>
                                            <p className="twm-job-address">Ankorondrano, Antananarivo</p>
                                            <a href="https://themeforest.net/user/thewebmax/portfolio" className="twm-job-websites site-text-primary">https://thewebmax.com</a>
                                        </div>
                                        <div className="twm-right-content">
                                            <div className="twm-jobs-amount"> Ar 45.000 - Ar 55.000 <span>/ Jour</span></div>
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-jobs-browse site-text-primary">Détail</NavLink>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-6">
                                    <div className="twm-jobs-grid-style1  m-b30">
                                        <div className="twm-media">
                                            <JobZImage src="images/jobs-company/pic3.jpg" alt="#" />
                                        </div>
                                        <span className="twm-job-post-duration">Il y a 6 mois</span>
                                        <div className="twm-jobs-category green"><span className="twm-bg-green">Alternance</span></div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-job-title">
                                                <h4 className="twm-job-title">Conseillier(ière) en relation client à distance</h4>
                                            </NavLink>
                                            <p className="twm-job-address">Ankadimbahoaka, Antananarivo</p>
                                            <a href="https://themeforest.net/user/thewebmax/portfolio" className="twm-job-websites site-text-primary">https://thewebmax.com</a>
                                        </div>
                                        <div className="twm-right-content">
                                            <div className="twm-jobs-amount">Ar 250.000 <span>/ Mois</span></div>
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-jobs-browse site-text-primary">Détail</NavLink>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-6">
                                    <div className="twm-jobs-grid-style1  m-b30">
                                        <div className="twm-media">
                                            <JobZImage src="images/jobs-company/pic5.jpg" alt="#" />
                                        </div>
                                        <span className="twm-job-post-duration">Il y a 1 jour</span> 
                                        <div className="twm-jobs-category green"><span className="twm-bg-yellow">Job-Etudiant</span></div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-job-title">
                                                <h4 className="twm-job-title">Manager point de vente - Tournoi de tennis</h4>
                                            </NavLink>
                                            <p className="twm-job-address">Ankadimbahoaka, Antananarivo</p>
                                            <a href="https://themeforest.net/user/thewebmax/portfolio" className="twm-job-websites site-text-primary">https://thewebmax.com</a>
                                        </div>
                                        <div className="twm-right-content">
                                            <div className="twm-jobs-amount">Ar 35.000 <span>/ Jour</span></div>
                                            <NavLink to={publicUser.jobs.DETAIL1} className="twm-jobs-browse site-text-primary">Détail</NavLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* JOB POST END */}
            {/* CANDIDATES START */}
            <div className="section-full p-t120 p-b90 site-bg-white">
                <div className="container">
                    {/* title="" START*/}
                    <div className="section-head center wt-small-separator-outer">
                        <div className="wt-small-separator site-text-primary">
                            <div>Candidats</div>
                        </div>
                        <h2 className="wt-title">Les candidats embauchés</h2>
                    </div>
                    {/* title="" END*/}
                    <div className="section-content">
                        <div className="twm-blog-post-3-outer-wrap">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-6 col-md-12 col-sm-12">
                                    {/*Block one*/}
                                    <div className="twm-candidates-list-style1">
                                        <div className="twm-media">
                                            <div className="twm-media-pic">
                                                <JobZImage src="images/candidates/pic1.jpg" alt="#" />
                                            </div>
                                            <div className="twm-candidates-tag"><span>Embauché</span></div>
                                        </div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.candidate.DETAIL1} className="twm-job-title">
                                                <h4>Anjara Andria</h4>
                                            </NavLink>
                                            <p>Rédacteur web</p>
                                            <div className="twm-fot-content">
                                                <div className="twm-left-info">
                                                    <p className="twm-candidate-address"><i className="feather-map-pin" />Antananarivo</p>
                                                    <div className="twm-jobs-vacancies">Ar 90.000<span>/ Jour</span></div>
                                                </div>
                                                <div className="twm-right-btn">
                                                    <NavLink to={publicUser.candidate.DETAIL1} className="twm-view-prifile site-text-primary">Voir le profil</NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12">
                                    {/*Block two*/}
                                    <div className="twm-candidates-list-style1">
                                        <div className="twm-media">
                                            <div className="twm-media-pic">
                                                <JobZImage src="images/candidates/pic2.jpg" alt="#" />
                                            </div>
                                            <div className="twm-candidates-tag"><span>Embauché</span></div>
                                        </div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.candidate.DETAIL1} className="twm-job-title">
                                                <h4>Laza Rakoto</h4>
                                            </NavLink>
                                            <p>Créateur de contenu miltimédia</p>
                                            <div className="twm-fot-content">
                                                <div className="twm-left-info">
                                                    <p className="twm-candidate-address"><i className="feather-map-pin" />Antananarivo</p>
                                                    <div className="twm-jobs-vacancies">Ar 30.000<span>/ Jour</span></div>
                                                </div>
                                                <div className="twm-right-btn">
                                                    <NavLink to={publicUser.candidate.DETAIL1} className="twm-view-prifile site-text-primary">Voir le profil</NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12">
                                    {/*Block three*/}
                                    <div className="twm-candidates-list-style1">
                                        <div className="twm-media">
                                            <div className="twm-media-pic">
                                                <JobZImage src="images/candidates/pic3.jpg" alt="#" />
                                            </div>
                                            <div className="twm-candidates-tag"><span>Embauché</span></div>
                                        </div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.candidate.DETAIL1} className="twm-job-title">
                                                <h4>John Rason</h4>
                                            </NavLink>
                                            <p>Caissier</p>
                                            <div className="twm-fot-content">
                                                <div className="twm-left-info">
                                                    <p className="twm-candidate-address"><i className="feather-map-pin" />Antananarivo</p>
                                                    <div className="twm-jobs-vacancies">Ar 35.000<span>/ Jour</span></div>
                                                </div>
                                                <div className="twm-right-btn">
                                                    <NavLink to={publicUser.candidate.DETAIL1} className="twm-view-prifile site-text-primary">Voir le profil</NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12">
                                    {/*Block three*/}
                                    <div className="twm-candidates-list-style1">
                                        <div className="twm-media">
                                            <div className="twm-media-pic">
                                                <JobZImage src="images/candidates/pic3.jpg" alt="#" />
                                            </div>
                                            <div className="twm-candidates-tag"><span>Embauché</span></div>
                                        </div>
                                        <div className="twm-mid-content">
                                            <NavLink to={publicUser.candidate.DETAIL1} className="twm-job-title">
                                                <h4>John Rason</h4>
                                            </NavLink>
                                            <p>Caissier</p>
                                            <div className="twm-fot-content">
                                                <div className="twm-left-info">
                                                    <p className="twm-candidate-address"><i className="feather-map-pin" />Antananarivo</p>
                                                    <div className="twm-jobs-vacancies">Ar 35.000<span>/ Jour</span></div>
                                                </div>
                                                <div className="twm-right-btn">
                                                    <NavLink to={publicUser.candidate.DETAIL1} className="twm-view-prifile site-text-primary">Voir le profil</NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div className="text-center m-b30">
                                <NavLink to={publicUser.candidate.LIST} className=" site-button">Tous les candidats</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* CANDIDATES END */}
            
        </>
    )
}

export default Home3Page;