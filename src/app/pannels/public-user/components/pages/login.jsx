import { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { NavLink, useNavigate } from "react-router-dom";
import { publicUrlFor } from "../../../../../globals/constants";
import { canRoute, candidate, empRoute, employer, publicUser } from "../../../../../globals/route-names";
import JobZImage from "../../../../common/jobz-img";

function LoginPage() {

    const navigate = useNavigate();
    const [canusername, setCanUsername] = useState('');
    const [empusername, setEmpUsername] = useState('');
    const [password, setPassword] = useState('');
    const [recaptcha, setRecaptcha] = useState(false);

    const onChange = () => {
        setRecaptcha(true);
    };


    const handleCandidateLogin = async (event) => {
        event.preventDefault();
        try {
            const response = await login('/login/loginStudent', { email: canusername, password });
            handleLoginResponse(response, moveToCandidate);
        } catch (error) {
            console.error('An error occurred during candidate login:', error);
        }
    };

    const handleEmployerLogin = async (event) => {
        event.preventDefault();
        try {
            const response = await login('/login/loginCompany', { email: empusername, password });
            handleLoginResponse(response, moveToEmployer);
        } catch (error) {
            console.error('An error occurred during employer login:', error);
        }
    };

    const login = async (endpoint, data) => {
        const response = await fetch(`http://localhost:8080${endpoint}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });
        return response;
    };

    const handleLoginResponse = async (response, successCallback) => {
        if (response.ok) {
            const userData = await response.json();
            successCallback();
        } else {
            console.log('error');
        }
    };

    const moveToCandidate = () => {
        navigate(canRoute(candidate.DASHBOARD));
    };

    const moveToEmployer = () => {
        navigate(empRoute(employer.DASHBOARD));
    };

    return (
        <>
            <div className="section-full site-bg-white">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-8 col-lg-6 col-md-5 twm-log-reg-media-wrap" style={{
                            backgroundImage: `url(${publicUrlFor("images/loginbg.jpg")})`,
                            backgroundSize: "cover",
                            backgroundPosition: "center",
                        }}>
                            <div className="twm-log-reg-media">
                                <div className="twm-l-media">

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-7">
                            <div className="twm-log-reg-form-wrap">
                                <div className="twm-log-reg-logo-head">
                                    <NavLink to={publicUser.INITIAL}>
                                        <JobZImage src="images/logo-12.png" alt="" className="logo" />
                                    </NavLink>
                                </div>
                                <div className="twm-log-reg-inner">
                                    <div className="twm-log-reg-head">
                                        <div className="twm-log-reg-logo">
                                            <span className="log-reg-form-title">Connexion</span>
                                        </div>
                                    </div>
                                    <div className="twm-tabs-style-2">
                                        <ul className="nav nav-tabs" id="myTab2" role="tablist">
                                            {/*Login Candidate*/}
                                            <li className="nav-item">
                                                <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#twm-login-candidate" type="button"><i className="fas fa-user-tie" />Etudiant</button>
                                            </li>
                                            {/*Login Employer*/}
                                            <li className="nav-item">
                                                <button className="nav-link" data-bs-toggle="tab" data-bs-target="#twm-login-Employer" type="button"><i className="fas fa-building" />Entreprise</button>
                                            </li>
                                        </ul>
                                        <div className="tab-content" id="myTab2Content">
                                            {/*Login Candidate Content*/}
                                            <form onSubmit={handleCandidateLogin} className="tab-pane fade show active" id="twm-login-candidate">
                                                <div className="row">
                                                    <div className="col-lg-12">
                                                        <div className="form-group mb-3">
                                                            <input name="username"
                                                                type="text"
                                                                required
                                                                className="form-control"
                                                                placeholder="Usearname*"
                                                                value={canusername}
                                                                onChange={(event) => {
                                                                    setCanUsername(event.target.value);
                                                                }} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12">
                                                        <div className="form-group mb-3">
                                                            <input
                                                                name="password"
                                                                type="password"
                                                                className="form-control"
                                                                required
                                                                placeholder="Password*"
                                                                value={password}
                                                                onChange={(event) => {
                                                                    setPassword(event.target.value);
                                                                }} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12">
                                                        <div className="twm-forgot-wrap">
                                                            <div className="form-group mb-3">
                                                                <div className="form-check">
                                                                    <input type="checkbox" className="form-check-input" id="Password4" />
                                                                    <label className="form-check-label rem-forgot" htmlFor="Password4">Se souvenir de moi <a href="#" className="site-text-primary">Mot de passe oubliÃ©</a></label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <ReCAPTCHA
                                                        sitekey="6Le4uEYpAAAAAOARDDh8R_JlDzKTeDA_gmQjLFox"
                                                        onChange={onChange}
                                                    />
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <button type="submit" className="site-button">Se connecter</button>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <span className="center-text-or">Ou</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <button type="submit" className="log_with_facebook">
                                                                <i className="fab fa-facebook" />
                                                                Continuer avec facebook
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <button type="submit" className="log_with_google">
                                                                <JobZImage src="images/google-icon.png" alt="" />
                                                                Continuer avec Google
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            {/*Login Employer Content*/}
                                            <form onSubmit={handleEmployerLogin} className="tab-pane fade" id="twm-login-Employer">
                                                <div className="row">
                                                    <div className="col-lg-12">
                                                        <div className="form-group mb-3">
                                                            <input
                                                                name="username"
                                                                type="text"
                                                                required
                                                                className="form-control"
                                                                placeholder="Usearname*"
                                                                value={empusername}
                                                                onChange={(event) => {
                                                                    setEmpUsername(event.target.value);
                                                                }} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12">
                                                        <div className="form-group mb-3">
                                                            <input
                                                                name="password"
                                                                type="password"
                                                                className="form-control"
                                                                required
                                                                placeholder="Password*"
                                                                value={password}
                                                                onChange={(event) => {
                                                                    setPassword(event.target.value);
                                                                }} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12">
                                                        <div className="twm-forgot-wrap">
                                                            <div className="form-group mb-3">
                                                                <div className="form-check">
                                                                    <input type="checkbox" className="form-check-input" id="Password4" />
                                                                    <label className="form-check-label rem-forgot" htmlFor="Password4">Se souvenir de moi <a href="#" className="site-text-primary">Mot de passe oubliÃ©</a></label>
                                                                </div>
                                                            </div>
                                                            <ReCAPTCHA
                                                                sitekey="6Le4uEYpAAAAAOARDDh8R_JlDzKTeDA_gmQjLFox"
                                                                onChange={onChange}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <button type="submit" className="site-button">Connexion</button>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <span className="center-text-or">Ou</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <button type="submit" className="log_with_facebook">
                                                                <i className="fab fa-facebook" />
                                                                Continuer avec facebook
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <button type="submit" className="log_with_google">
                                                                <JobZImage src="images/google-icon.png" alt="" />
                                                                Continuer avec google
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default LoginPage;