import { useEffect, useState } from "react";

function ContactUsPage() {

    const [nom,setNom]= useState("")
    const [email,setEmail] = useState("")
    const [telephone,setTelephone] = useState("")
    const [objet,setObjet] = useState("")
    const [message, setMessage] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const [successMessage, setSuccessMessage] = useState("")

    const handleSubmit = (event) => {
        event.preventDefault()

    // Vérifier si tous les champs sont remplis
        if (!nom || !email || !telephone || !objet || !message) {
        setErrorMessage("Tous les champs doivent être remplis.")
      return
    }

    // Vérifier si l'email est valide
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setErrorMessage("Veuillez saisir une adresse e-mail valide.")
      return
    }

    // Envoyer le formulaire
    sendFeedback("template_ajohxgh", {
        nom,
        email,
        telephone,
        objet,
        message,
    })
}

    const sendFeedback = (templateId, variables) => {
        window.emailjs
            .send("service_imo0d43", templateId, variables, "1PHtK28v-F-qpcCWy")
            .then((res) => {
                console.log("Mety!")
                setNom("")
                setEmail("");
                setTelephone("")
                setObjet("")
                setMessage("")
                setErrorMessage("")
                setSuccessMessage("Votre message a été envoyé avec succès !")
            }, 2000)
            .catch((err) => {
                console.error("Error:", err);
                const formMessage = document.querySelector(".form-message");
                if (formMessage) {
                    formMessage.textContent = "Une erreur s'est produite, veuillez réessayer.";
                } else {
                    alert("Une erreur s'est produite, veuillez réessayer.");
                }
            });
    };
   
    return (
        <>
            <div className="section-full twm-contact-one">
                <div className="section-content">
                    <div className="container">
                        {/* CONTACT FORM*/}
                        <div className="contact-one-inner">
                            <div className="row">
                                <div className="col-lg-6 col-md-12">
                                    <div className="contact-form-outer">
                                        {/* title="" START*/}
                                        <div className="section-head left wt-small-separator-outer">
                                            <h2 className="wt-title">Envoyer nous un message </h2>
                                            <p>N'hésitez pas à nous contacter et nous vous répondrons dès que possible.</p>
                                        </div>
                                        <div className="form-success-message">{successMessage}</div> <br />
                                        {/* title="" END*/}
                                        <form className="cons-contact-form" method="post">
                                            <div className="row">
                                                <div className="col-lg-6 col-md-6">
                                                    <div className="form-group mb-3">
                                                        <input 
                                                        value={nom} 
                                                        onChange={event=> setNom(event.target.value)} 
                                                        name="nom" 
                                                        type="text"
                                                        //autoComplete="off"
                                                        required 
                                                        className="form-control" 
                                                        placeholder="Nom et Prénom" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6">
                                                    <div className="form-group mb-3">
                                                        <input value={email} 
                                                        onChange={e => setEmail(e.target.value)}  
                                                        name="email" 
                                                        type="email" 
                                                        className="form-control" 
                                                        autoCapitalize="off"
                                                        required 
                                                        placeholder="Email" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6">
                                                    <div className="form-group mb-3">
                                                        <input 
                                                        value={telephone} 
                                                        onChange={event => setTelephone(event.target.value)} 
                                                        name="phone" 
                                                        type="text" 
                                                        className="form-control" 
                                                        required 
                                                        placeholder="Téléphone" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6">
                                                    <div className="form-group mb-3">
                                                        <input 
                                                        value={objet} 
                                                        onChange={event => setObjet(event.target.value)} 
                                                        name="subject" 
                                                        type="text" 
                                                        className="form-control" 
                                                        required 
                                                        placeholder="Objet" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <textarea 
                                                        value={message} 
                                                        onChange={event => setMessage(event.target.value)} 
                                                        name="message" 
                                                        className="form-control" 
                                                        rows={3} 
                                                        autoComplete="on"
                                                        placeholder="Message" 
                                                        defaultValue={""} />
                                                    </div>
                                                </div>
                                                <div class="form-message"></div> <br />
                                                <div className="col-md-12">
                                                    <button
                                                    onClick={handleSubmit}
                                                    type="submit" 
                                                    className="site-button">Envoyer</button> 
                                                </div> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12">
                                    <div className="contact-info-wrap">
                                        <div className="contact-info">
                                            <div className="contact-info-section">
                                                <div className="c-info-column">
                                                    <div className="c-info-icon"><i className=" fas fa-map-marker-alt" /></div>
                                                    <h3 className="twm-title">Où nous trouver?</h3>
                                                    <p>II 62 B Faravohitra Antananarivo, 101</p>
                                                </div>
                                                <div className="c-info-column">
                                                    <div className="c-info-icon custome-size"><i className="fas fa-mobile-alt" /></div>
                                                    <h3 className="twm-title">Contactez-nous</h3>
                                                    <p><a href="">+261 32 95 877 75</a></p>
                                                    <p><a href="1">+261 33 85 766 93</a></p>
                                                </div>
                                                <div className="c-info-column">
                                                    <div className="c-info-icon"><i className="fas fa-envelope" /></div>
                                                    <h3 className="twm-title">Adresse e-mail</h3>
                                                    <p>aody.inclusiv@gmail.com</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="gmap-outline">
                <div className="google-map">
                    <div style={{ width: '100%' }}>
                        <iframe height={460} src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA2utLDvU8IhZtrE3qjg7kGApB7tJT_n2M&q=II+62+B+Faravohitra+Antananarivo+101" />
                    </div> 
                </div>
            </div>
        </>
    )
}

export default ContactUsPage;