import { defaults } from "chart.js";
import JobZImage from "../../../../common/jobz-img";

function SectionHowItWorks() {
    return (
        <>
            <div className="section-full p-t120 p-b90 site-bg-white twm-how-it-work-area2" style={{backgroundColor:"#FFFFEC"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-12">
                            {/* title="" START*/}
                            <div className="section-head left wt-small-separator-outer">
                                <div className="wt-small-separator site-text-primary">
                                    
                                </div>
                                <h2 className="wt-title">Suivez nos étapes, nous vous aiderons.</h2>
                            </div>
                            <ul className="description-list">
                                <li>
                                    <i className="feather-check" />
                                    Emploi de confiance et de qualité
                                </li>
                                <li>
                                    <i className="feather-check" />
                                    Emploi international
                                </li>
                                <li>
                                    <i className="feather-check" />
                                    Aucun frais supplémentaire
                                </li>
                                <li>
                                    <i className="feather-check" />
                                    Les meilleures entreprises
                                </li>
                            </ul>
                            {/* title="" END*/}
                        </div>
                        <div className="col-lg-8 col-md-12">
                            <div className="twm-w-process-steps-2-wrap">
                                <div className="row">
                                    <div className="col-xl-6 col-lg-6 col-md-6">
                                        <div className="twm-w-process-steps-2">
                                            <div className="twm-w-pro-top bg-clr-sky-light bg-sky-light-shadow">
                                                <span className="twm-large-number text-clr-sky">01</span>
                                                <div className="twm-media">
                                                    <span><JobZImage src="images/work-process/icon1.png" alt="icon1" /></span>
                                                </div>
                                                <h4 className="twm-title">Créez<br />votre compte</h4>
                                                <p>Vous devez créer un compte pour trouver le meilleur emploi de votre choix.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6">
                                        <div className="twm-w-process-steps-2">
                                            <div className="twm-w-pro-top bg-clr-yellow-light bg-yellow-light-shadow">
                                                <span className="twm-large-number text-clr-yellow">02</span>
                                                <div className="twm-media">
                                                    <span><JobZImage src="images/work-process/icon4.png" alt="icon1" /></span>
                                                </div>
                                                <h4 className="twm-title">Recherchez<br />
                                                    votre emploi</h4>
                                                <p>Ayant un compte, vous pouvez raffiner vos recherches pour une meilleure expérience.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6">
                                        <div className="twm-w-process-steps-2">
                                            <div className="twm-w-pro-top bg-clr-pink-light bg-pink-light-shadow">
                                                <span className="twm-large-number text-clr-pink">03</span>
                                                <div className="twm-media">
                                                    <span><JobZImage src="images/work-process/icon3.png" alt="icon1" /></span>
                                                </div>
                                                <h4 className="twm-title">Postulez<br />pour votre emploi de rêve</h4>
                                                <p>Envoyez votre candidature selon votre disponibilité</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6">
                                        <div className="twm-w-process-steps-2">
                                            <div className="twm-w-pro-top bg-clr-green-light bg-clr-light-shadow">
                                                <span className="twm-large-number text-clr-green">04</span>
                                                <div className="twm-media">
                                                    <span><JobZImage src="images/work-process/icon3.png" alt="icon1" /></span>
                                                </div>
                                                <h4 className="twm-title">Faites vous<br />remarquer</h4>
                                                <p>Mettez à jour votre profil pour attirer les recruteurs</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="twm-how-it-work-section">
                    </div>
                </div>
            </div>
        </>
    )
}
export default SectionHowItWorks