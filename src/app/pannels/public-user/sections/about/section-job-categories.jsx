import { NavLink } from "react-router-dom";
import { publicUser } from "../../../../../globals/route-names";

function SectionJobCategories() {
    return (
        <>
            <div className="section-full p-t120 p-b90 site-bg-gray twm-job-categories-area2" style={{textAlign: "center"}}>
                {/* title="" START*/}
                <div className="section-head center wt-small-separator-outer">
                    <div className="wt-small-separator site-text-primary" style={{height:"80px", marginBlockStart:"-50px" }}>
                        <div style={{ fontSize: "24px", height: "50px", width:"500px"}}>Objectifs du plateforme</div>
                    </div>
                    {/* <h2 className="wt-title">Choose Your Desire Category</h2> */}
                </div>
                {/* title="" END*/}
                <div style={{ height: "100px", width: "800px", marginLeft: "400px", color: "gray", marginBlock:"-80px", marginBlockEnd:"-50px" }}>
                    <p style={{ fontSize: "18px", fontWeight: "normal"}}>
                        "Nous travaillons en partenariat étroit avec des  <b>entreprises de différents secteurs</b> pour comprendre leurs besoins en matière de recrutement et <b>leur proposer des candidats talentueux et motivés</b>. Notre plateforme offre aux entreprises une visibilité accrue auprès d'une large base d' <b>étudiants qualifiés</b>, leur permettant ainsi de trouver les <b>meilleurs candidats</b> pour leurs offres."
                    </p>  
                </div>
                
            </div>
        </>
    )
}

export default SectionJobCategories;