import { NavLink } from "react-router-dom";
import JobZImage from "../../../../common/jobz-img";
import { publicUser } from "../../../../../globals/route-names";

function SectionTopCompanies() {
    return (
        <>
            <div  className="section-full p-t120  site-bg-white twm-companies-wrap" style={{backgroundColor:"#FFFFEC"}}>
                {/* title="" START*/}
                <div className="section-head center wt-small-separator-outer">
                    <h2 className="wt-small-separator site-text-primary" style={{ fontSize: "24px" }}>Fonctionnalités clés de Aody</h2>
                </div>
                <div className="twm-title-large">
                    <div className="logo-wrapper">
                        <JobZImage src="images/logo-12.png" style={{ width: "300px", marginLeft: "550px" }} />
                    </div>
                    <h4 style={{ width: "900px", height: "100px", marginLeft: "300px", color: "gray", textAlign:"center" }}>
                        <br></br> 
                        "Découvrez comment notre plateforme a aidé des étudiants à décrocher 
                        des stages de rêve et comment des entreprises ont trouvé des talents 
                        prometteurs pour rejoindre leurs équipes."
                    </h4>
                    <div style={{ width: "900px", height: "350px", marginLeft: "300px", fontSize: "18px", color: "gray", textAlign:"center" }}>
                        <p>
                            Notre <b>plateforme</b> propose une recherche avancée qui permet aux étudiants 
                            de trouver rapidement les opportunités qui correspondent à leurs critères spécifiques. 
                            Les profils personnalisés permettent aux entreprises de présenter leurs offres de manière détaillée, 
                            tandis que nos algorithmes de recommandation aident à connecter les étudiants avec les entreprises 
                            qui correspondent le mieux à leurs aspirations.
                        </p>
                        <p>
                            En utilisant <b>notre plateforme</b>, les étudiants bénéficieront d'un accès privilégié à des opportunités de stage, 
                            d'apprentissage et d'emploi provenant d'un large réseau d'entreprises partenaires.
                            Notre processus de candidature simplifié leur permettra de postuler facilement et efficacement, 
                            augmentant ainsi leurs chances de réussite dans leur recherche d'opportunités professionnelles.
                        </p>
                    </div>
                </div>
                {/* title="" END*/}
            </div>
        </>
    )
}

export default SectionTopCompanies;