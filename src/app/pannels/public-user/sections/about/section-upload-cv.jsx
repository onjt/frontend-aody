import { publicUrlFor } from "../../../../../globals/constants";
import JobZImage from "../../../../common/jobz-img";

function SectionUploadCV() {
    return (
        <div className="section-full p-t120 p-b120 twm-explore-area bg-cover" >
            <div className="container">
                <div className="section-content">
                    <div className="row">
                        <div className="col-lg-4 col-md-12">
                            <div className="twm-explore-media-wrap">
                                {/* <div className="twm-media">
                                    <JobZImage src="images/student.png" alt="" style={{ width: "100%", height: "auto" }} />
                                </div> */}
                            </div>
                        </div>
                        <div className="col-lg-8 col-md-12">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SectionUploadCV;