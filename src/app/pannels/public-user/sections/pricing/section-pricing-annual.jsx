import { NavLink } from "react-router-dom";
import { publicUser } from "../../../../../globals/route-names";
import { useState,useEffect } from "react";

function SectionPricingAnnual() {
    const [storedForfaits, setStoredForfaits] = useState([]);

    useEffect(() => {
        // Récupérer la liste des forfaits depuis la session
        const storedForfaitsString = sessionStorage.getItem('forfaits');
        if (storedForfaitsString) {
            const storedForfaitsArray = JSON.parse(storedForfaitsString);
            setStoredForfaits(storedForfaitsArray);
        }
    }, []);
    return (
        <>
            <div className="pricing-block-outer">
                <div className="row justify-content-center">
                {storedForfaits.map(storedForfait => (
                    <div className={`col-lg-4 col-md-6 m-b30  ${storedForfait.idPackage === 1 ? 'p-table-highlight': ''}`}>
                    <div className={`pricing-table-1  ${storedForfait.idPackage === 1 ? 'circle-yellow' : (storedForfait.idPackage === 2 ? 'circle-pink' : '')}`}>
                        <div className="p-table-title">
                            <h4 className="wt-title">
                                {storedForfait.packageName}
                            </h4>
                        </div>
                        <div className="p-table-inner">
                            <div className="p-table-price">
                                <span>Ar {storedForfait.price *11}/</span>
                                <p>An</p>
                            </div>
                            <div className="p-table-list">
                                <ul>
                                    <li className={storedForfait.jobPosting > 0 ? '' : 'disable'}>
                                        <i className={` ${storedForfait.jobPosting > 0 ? 'feather-check' : 'feather-x'}`} />
                                        {storedForfait.jobPosting*12} postes
                                    </li>
                                    <li className={storedForfait.boostJob > 0 ? '' : 'disable'}>
                                        <i className={` ${storedForfait.boostJob > 0 ? 'feather-check' : 'feather-x'}`} />
                                        {storedForfait.boostJob*12} boosteurs
                                    </li>
                                    <li className={storedForfait.jobDisplayed > 0 ? '' : 'disable'}>
                                        <i className={` ${storedForfait.jobDisplayed > 0 ? 'feather-check' : 'feather-x'}`} />
                                        {storedForfait.jobDisplayed+1} semaines d'affichage de la publication
                                    </li>
                                    <li className={storedForfait.premiumSupport !== false ? '' : 'disable'}>
                                        <i className={` ${storedForfait.premiumSupport !== false ? 'feather-check' : 'feather-x'}`} />
                                        {storedForfait.premiumSupport} Support premium
                                    </li>
                                </ul>
                            </div>
                            <div className="p-table-btn">
                                <NavLink to={publicUser.pages.ABOUT} className="site-button">S'abonner</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
                ))}   
                </div>
            </div>
        </>
    )
}

export default SectionPricingAnnual;