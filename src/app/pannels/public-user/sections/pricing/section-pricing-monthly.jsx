import { NavLink } from "react-router-dom";
import { publicUser } from "../../../../../globals/route-names";
import { useEffect,useState } from "react";
import axios from "axios";

function SectionPricingMonthly() {
    const [forfaits, setForfaits] = useState([]);

    useEffect(() => {
        // Appeler l'API Spring pour récupérer la liste des forfaits
        axios.get('http://localhost:8080/package/list')
            .then(response => {
                setForfaits(response.data);
                // Stocker la liste des forfaits dans la session
                sessionStorage.setItem('forfaits', JSON.stringify(response.data));
            })
            .catch(error => console.error('Erreur lors de la récupération des forfaits', error));
    }, []);
    return (
        <>
            <div className="pricing-block-outer">
                <div className="row justify-content-center">
                {forfaits.map(forfait => (
                    <div className={`col-lg-4 col-md-6 m-b30  ${forfait.idPackage === 1 ? 'p-table-highlight': ''}`}>
                    <div className={`pricing-table-1  ${forfait.idPackage === 1 ? 'circle-yellow' : (forfait.idPackage === 2 ? 'circle-pink' : '')}`}>
                        <div className="p-table-title">
                            <h4 className="wt-title">
                                {forfait.packageName}
                            </h4>
                        </div>
                        <div className="p-table-inner">
                            <div className="p-table-price">
                                <span>Ar {forfait.price}/</span>
                                <p>Mois</p>
                            </div>
                            <div className="p-table-list">
                                <ul>
                                    <li className={forfait.jobPosting > 0 ? '' : 'disable'}>
                                        <i className={` ${forfait.jobPosting > 0 ? 'feather-check' : 'feather-x'}`} />
                                        {forfait.jobPosting} postes
                                    </li>
                                    <li className={forfait.boostJob > 0 ? '' : 'disable'}>
                                        <i className={` ${forfait.boostJob > 0 ? 'feather-check' : 'feather-x'}`} />
                                        {forfait.boostJob} boosteurs
                                    </li>
                                    <li className={forfait.jobDisplayed > 0 ? '' : 'disable'}>
                                        <i className={` ${forfait.jobDisplayed > 0 ? 'feather-check' : 'feather-x'}`} />
                                        {forfait.jobDisplayed} semaines d'affichage de la publication
                                    </li>
                                    <li className={forfait.premiumSupport !== false ? '' : 'disable'}>
                                        <i className={` ${forfait.premiumSupport !== false ? 'feather-check' : 'feather-x'}`} />
                                        {forfait.premiumSupport} Support premium
                                    </li>
                                </ul>
                            </div>
                            <div className="p-table-btn">
                                <NavLink to={publicUser.pages.ABOUT} className="site-button">S'abonner</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
                ))}   
                </div>
            </div>
        </>
    )
}

export default SectionPricingMonthly;