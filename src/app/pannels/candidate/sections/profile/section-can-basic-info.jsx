import React, { useEffect, useState } from "react";

function SectionCandidateBasicInfo() {
    const initialData = JSON.parse(sessionStorage.getItem('candidateData')) || {};
    const [name, setName] = useState(initialData.name || "");
    const [phone, setPhone] = useState(initialData.phone || "");
    const [email, setEmail] = useState(initialData.email || "");
    const [website, setWebsite] = useState(initialData.website || "");
    const [qualification, setQualification] = useState(initialData.qualification || "");
    const [language, setLanguage] = useState(initialData.language || "");
    const [jobCategory, setJobCategory] = useState(initialData.jobCategory || "");
    const [experience, setExperience] = useState(initialData.experience || "");
    const [currentSalary, setCurrentSalary] = useState(initialData.currentSalary || "");
    const [expectedSalary, setExpectedSalary] = useState(initialData.expectedSalary || "");
    const [age, setAge] = useState(initialData.age || "");
    const [country, setCountry] = useState(initialData.country || "");
    const [city, setCity] = useState(initialData.city || "");
    const [postCode, setPostCode] = useState(initialData.postcode || "");
    const [address, setAddress] = useState(initialData.fullAddress || "");
    const [description, setDescription] = useState(initialData.descriptionStudent || "");

    useEffect(() => {
        // Vous pouvez utiliser les données initiales ici si nécessaire
    }, [initialData]);

    const handleSubmit = (event) => {
        event.preventDefault();
        const candidateData = {
            name,
            phone,
            email,
            website,
            qualification,
            language,
            jobCategory,
            experience,
            currentSalary,
            expectedSalary,
            age,
            country,
            city,
            postCode,
            address,
            description,
        };

        // Ajoutez votre logique de gestion des données ici, par exemple, stocker les données en session
        sessionStorage.setItem('candidateData', JSON.stringify(candidateData));
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className="panel panel-default">
                <div className="panel-heading wt-panel-heading p-a20">
                    <h4 className="panel-tittle m-a0">Informations de base</h4>
                </div>
                <div className="panel-body wt-panel-body p-a20 m-b30">
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group">
                                <label>Votre nom</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_name"
                                        type="text"
                                        placeholder="Saisissez votre nom"
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-user " />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group">
                                <label>Téléphone</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_phone"
                                        type="text"
                                        placeholder="Saisissez votre numéro de téléphone"
                                        value={phone}
                                        onChange={(e) => setPhone(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-phone-alt" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group">
                                <label>Adresse e-mail</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_email"
                                        type="email"
                                        placeholder="Saisissez votre adresse e-mail"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                    />
                                    <i className="fs-input-icon fas fa-at" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group">
                                <label>Site Web</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_website"
                                        type="text"
                                        placeholder="Saisissez votre site Web"
                                        value={website}
                                        onChange={(e) => setWebsite(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-globe-americas" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group">
                                <label>Qualification</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_qualification"
                                        type="text"
                                        placeholder="Saisissez votre qualification"
                                        value={qualification}
                                        onChange={(e) => setQualification(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-user-graduate" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group">
                                <label>Langue</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_language"
                                        type="text"
                                        placeholder="e.x Anglais, Espagnol"
                                        value={language}
                                        onChange={(e) => setLanguage(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-language" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Catégorie d'emploi</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_job_category"
                                        type="text"
                                        placeholder="IT & Logiciel"
                                        value={jobCategory}
                                        onChange={(e) => setJobCategory(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-border-all" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Expérience</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_experience"
                                        type="text"
                                        placeholder="05 ans"
                                        value={experience}
                                        onChange={(e) => setExperience(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-user-edit" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Salaire actuel</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_current_salary"
                                        type="text"
                                        placeholder="65K"
                                        value={currentSalary}
                                        onChange={(e) => setCurrentSalary(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-dollar-sign" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Salaire attendu</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_expected_salary"
                                        type="text"
                                        placeholder="75K"
                                        value={expectedSalary}
                                        onChange={(e) => setExpectedSalary(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-dollar-sign" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Âge</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_age"
                                        type="text"
                                        placeholder="35 ans"
                                        value={age}
                                        onChange={(e) => setAge(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-child" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Pays</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_country"
                                        type="text"
                                        placeholder="États-Unis"
                                        value={country}
                                        onChange={(e) => setCountry(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-globe-americas" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Ville</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_city"
                                        type="text"
                                        placeholder="Texas"
                                        value={city}
                                        onChange={(e) => setCity(e.target.value)}
                                    />
                                    <i className="fs-input-icon fa fa-globe-americas" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Code postal</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_postcode"
                                        type="text"
                                        placeholder="75462"
                                        value={postCode}
                                        onChange={(e) => setPostCode(e.target.value)}
                                    />
                                    <i className="fs-input-icon fas fa-map-pin" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-12 col-lg-12 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Adresse complète</label>
                                <div className="ls-inputicon-box">
                                    <input
                                        className="form-control"
                                        name="candidate_address"
                                        type="text"
                                        placeholder="1363-1385 Sunset Blvd Angeles, CA 90026, États-Unis"
                                        value={address}
                                        onChange={(e) => setAddress(e.target.value)}
                                    />
                                    <i className="fs-input-icon fas fa-map-marker-alt" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Description</label>
                                <textarea
                                    className="form-control"
                                    rows={3}
                                    value={description}
                                    onChange={(e) => setDescription(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12">
                            <div className="text-left">
                                <button type="submit" className="site-button">
                                    Enregistrer les modifications
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
}

export default SectionCandidateBasicInfo;
