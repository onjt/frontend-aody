import { NavLink, useLocation, useNavigate } from "react-router-dom";
import { setMenuActive } from "../../../../../globals/constants";
import { canRoute, candidate, publicUser } from "../../../../../globals/route-names";
import JobZImage from "../../../../common/jobz-img";

function CanSidebarSection() {
    const initialData = JSON.parse(sessionStorage.getItem('candidateData')) || {};
    const name = initialData.name || "";
    const jobCategory = initialData.jobCategory || "";

    const currentpath = useLocation().pathname;

    const navigate = useNavigate();
    
    const handleLogout = () => {
        sessionStorage.clear();
        navigate(`${publicUser.HOME3}`);
        window.location.reload()
    };

    return (
        <>
            <div className="twm-candidate-profile-pic">
                <JobZImage src="images/user-avtar/pic4.jpg" alt="" />
                <div className="upload-btn-wrapper">
                    <div id="upload-image-grid" />
                    <button className="site-button button-sm">Ajouter une photo</button>
                    <input type="file" name="myfile" id="file-uploader" accept=".jpg, .jpeg, .png" />
                </div>
            </div>
            <div className="twm-mid-content text-center">
                <NavLink to={canRoute(publicUser.candidate.DETAIL1)} className="twm-job-title">
                    <h4>{name}</h4>
                </NavLink>
                <p>{jobCategory}</p>
            </div>
            <div className="twm-nav-list-1">
                <ul>
                    <li className={setMenuActive(currentpath, canRoute(candidate.DASHBOARD))}>
                        <NavLink to={canRoute(candidate.DASHBOARD)}><i className="fa fa-tachometer-alt" />
                            Dashboard
                        </NavLink>
                    </li>
                    <li className={setMenuActive(currentpath, canRoute(candidate.PROFILE))}>
                        <NavLink to={canRoute(candidate.PROFILE)}><i className="fa fa-user" />
                            Mon Profil
                        </NavLink>
                    </li>
                    <li className={setMenuActive(currentpath, canRoute(candidate.APPLIED_JOBS))}>
                        <NavLink to={canRoute(candidate.APPLIED_JOBS)}><i className="fa fa-suitcase" />
                            Mes candidatures
                        </NavLink>
                    </li>
                    <li className={setMenuActive(currentpath, canRoute(candidate.RESUME))}>
                        <NavLink to={canRoute(candidate.RESUME)}><i className="fa fa-receipt" />
                            Mon CV
                        </NavLink>
                    </li>
                    <li className={setMenuActive(currentpath, canRoute(candidate.SAVED_JOBS))}>
                        <NavLink to={canRoute(candidate.SAVED_JOBS)}><i className="fa fa-file-download" />
                            Enregistré
                        </NavLink>
                    </li>
                    
                    <li className={setMenuActive(currentpath, canRoute(candidate.ALERTS))}>
                        <NavLink to={canRoute(candidate.ALERTS)}><i className="fa fa-bell" />
                            Alerte Job
                        </NavLink>
                    </li>
                    <li className={setMenuActive(currentpath, canRoute(candidate.CHANGE_PASSWORD))}>
                        <NavLink to={canRoute(candidate.CHANGE_PASSWORD)}><i className="fa fa-fingerprint" />
                            Securité
                        </NavLink>
                    </li>
                    <li>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#logout-dash-profile" onClick={handleLogout}>
                                <i className="fa fa-share-square" />
                                <span className="admin-nav-text">Déconnexion</span>
                            </a>
                    </li>
                </ul>
            </div>
        </>
    )
}

export default CanSidebarSection;