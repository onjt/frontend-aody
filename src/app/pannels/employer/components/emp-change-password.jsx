function EmpChangePasswordPage() {
    return (
        <>
            {/* <div className="twm-right-section-panel site-bg-gray"> */}
             <div className="wt-admin-right-page-header clearfix">
                <h2>Changement mot de passe</h2>
                <div className="breadcrumbs"><a href="#">Accueil</a><a href="#">Tableau de bord</a><span>Changement mot de passe</span></div>
            </div>
                    {/*Basic Information*/}
                <form>
                    <div className="panel panel-default site-bg-white m-t30">
                        <div className="panel-heading wt-panel-heading p-a20">
                            <h4 className="panel-tittle m-a0"><i className="fa fa-suitcase" />Sécurité</h4>
                        </div>
                    {/* <div className="panel panel-default">
                        <div className="panel-heading wt-panel-heading p-a20">
                            <h4 className="panel-tittle m-a0"><i className="fa fa-suitcase" />Change Password</h4>
                        </div> */}
                            <div className="panel-body wt-panel-body p-a20 ">
                                <div className="row">
                                    <div className="col-lg-6 col-md-6">
                                        <div className="form-group">
                                            <label>Ancien mot de passe</label>
                                            <div className="ls-inputicon-box">
                                                <input className="form-control wt-form-control" name="company_name" type="password" placeholder />
                                                <i className="fs-input-icon fa fa-asterisk " />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6">
                                        <div className="form-group">
                                            <label>Nouveau mot de passe</label>
                                            <div className="ls-inputicon-box">
                                                <input className="form-control wt-form-control" name="company_name" type="password" placeholder />
                                                <i className="fs-input-icon fa fa-asterisk" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12">
                                        <div className="form-group">
                                            <label>Confirmer le nouveau mot de passe</label>
                                            <div className="ls-inputicon-box">
                                                <input className="form-control wt-form-control" name="company_name" type="password" placeholder />
                                                <i className="fs-input-icon fa fa-asterisk" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-12 col-lg-12 col-md-12">
                                        <div className="text-left">
                                            <button type="submit" className="site-button">Enregistrer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </>
        )
    }

export default EmpChangePasswordPage;