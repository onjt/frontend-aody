import axios from 'axios';
import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './UpdateJobModal.css';

function UpdateJobModal({ show, onHide, onUpdate, offer }) {
    const [updatedOffer, setUpdatedOffer] = useState(offer ? { ...offer } : {});

    useEffect(() => {
        setUpdatedOffer(offer ? { ...offer } : {});
    }, [offer]);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setUpdatedOffer({ ...updatedOffer, [name]: value });
    };

    const handleDateChange = (date, fieldName) => {
        setUpdatedOffer({ ...updatedOffer, [fieldName]: date });
    };

    const handleUpdate = async () => {
        try {
            const response = await axios.put(`http://localhost:8080/joboffers/updates/${offer.idOffer}`, updatedOffer);
            console.log('Réponse du backend:', response.data);
    
            // Appele de la fonction onUpdate du parent avec l'offre mise à jour
            onUpdate(updatedOffer);
            onHide();
        } catch (error) {
            console.error('Erreur lors de la mise à jour de l offre:', error);
        }
        window.location.reload()
    };
    

    return (
        <div show={show} onHide={onHide} className="custom-modal">
            <div className="custom-modal-header">
                <div className="custom-modal-title">Modifier l'offre d'emploi</div>
            </div>
            <div className="custom-modal-body">
                <label>Titre du poste</label>
                <input
                    className="form-control"
                    name="jobTitle"
                    type="text"
                    value={updatedOffer.jobTitle}
                    onChange={handleInputChange}
                />

                <label>Catégorie d'emploi</label>
                <input
                    className="form-control"
                    name="jobCategory"
                    type="text"
                    value={updatedOffer.jobCategory}
                    onChange={handleInputChange}
                />

                <label>Type d'emploi</label>
                <input
                    className="form-control"
                    name="jobType"
                    type="text"
                    value={updatedOffer.jobType}
                    onChange={handleInputChange}
                />

                <label>Description du poste</label>
                <textarea
                    className="form-control"
                    name="jobDescription"
                    rows={4}
                    value={updatedOffer.jobDescription}
                    onChange={handleInputChange}
                />

                <label>Salaire proposé</label>
                <input
                    className="form-control"
                    name="offeredSalary"
                    type="text"
                    value={updatedOffer.offeredSalary}
                    onChange={handleInputChange}
                />

                <label>Expérience requise</label>
                <input
                    className="form-control"
                    name="experience"
                    type="text"
                    value={updatedOffer.experience}
                    onChange={handleInputChange}
                />

                <label>Qualification requise</label>
                <input
                    className="form-control"
                    name="qualification"
                    type="text"
                    value={updatedOffer.qualification}
                    onChange={handleInputChange}
                />

                <label>Date de début</label>
                <DatePicker
                    selected={updatedOffer.startDateJobOffer instanceof Date ? updatedOffer.startDateJobOffer : null}
                    onChange={(date) => handleDateChange(date, 'startDateJobOffer')}
                    dateFormat="dd-MM-yyyy"
                    placeholderText="jj-mm-aaaa"
                />

                <label>Date de fin</label>
                <DatePicker
                    selected={updatedOffer.endDateJobOffer instanceof Date ? updatedOffer.endDateJobOffer : null}
                    onChange={(date) => handleDateChange(date, 'endDateJobOffer')}
                    dateFormat="dd-MM-yyyy"
                    placeholderText="jj-mm-aaaa"
                />
                
            </div>
                <label>Status de l Offre</label>
                    
                        <select
                            className="wt-select-box selectpicker"
                            data-live-search="true"
                            title=""
                            id="statusOffre"
                            data-bv-field="size"
                            name="statusOffer"
                            value={updatedOffer.statusOffer}
                            onChange={handleInputChange}
                            >
                            <option className="bs-title-option" value>
                                statusOffre
                            </option>
                            <option>Enregistrer</option>
                            <option>Poster</option>
                            <option>Fermer</option>
                        </select>
                                    
            <div className="custom-modal-footer">
                <button variant="primary" onClick={handleUpdate} className="primary">
                    Enregistrer les modifications
                </button>
            </div>
        </div>
    );
}

export default UpdateJobModal;
