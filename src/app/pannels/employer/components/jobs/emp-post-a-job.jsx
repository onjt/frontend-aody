import axios from 'axios';
import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import UpdateJobModal from './UpdateJobModal ';


//mila miistalle an ito zala a satria ampiasa bibliothèque de calendrier isika:   npm install react-datepicker
function EmpPostAJobPage() {
    const initialData = JSON.parse(sessionStorage.getItem('employerData')) || {};

    const citiesInMadagascar = [
        "Antananarivo",
        "Toamasina",
        "Antsirabe",
        "Fianarantsoa",
        "Tulear",
        "Manakara",
        "Mahajanga",
        "Diego",
        "Antalaha"
    ];

    const jobCategories = [
        "Comptabilité et finance",
        "Call center et saisie de données",
        "Informatique",
        "Conseil/orientation",
        "Administration judiciaire",
        "Hotellerie"
    ];

    const jobTypes = [
        "Temps plein",
        "Freelance/travailleur indépendant",
        "Temps partiel",
        "Stage",
        "Temporaire",
    ];
    
    const salaryOptions = [
        "10000.0",
        "20000.0",
        "30000.0",
        "40000.0",
        "50000.0",
    ];
    
    // Maka ny lera @izao fotoana izao
    const currentDate = new Date();
    console.log(currentDate)
    // Foramtentsika ho 'yyyy-MM-dd HH:mm:ss'
    const formattedDate = currentDate.toISOString();

    const [jobOffer, setJobOffer] = useState({
        jobTitle: '',
        jobCategory: '',
        jobType: '',
        jobDescription: '',
        offeredSalary: '',
        experience: '',
        qualification: '',
        requirements: '',
        responsabilities: '',
        city: '',
        location: '',
        latitude: '',
        country: '',
        longitude: '',
        startDateJobOffer: '',
        endDateJobOffer: '',
        publicationDate: formattedDate,
        statusOffer: '',
        company: {
            idAccount: initialData.idAccount || 0,
            email: '',
            website: '',
            since: '',
            address: ''
        },
    });

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setJobOffer({ ...jobOffer, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.post('http://localhost:8080/joboffers/saves', jobOffer);

            console.log('Réponse du backend:', response.data);

            setJobOffer({
                jobTitle: '',
                jobCategory: '',
                jobType: '',
                jobDescription: '',
                offeredSalary: '',
                experience: '',
                qualification: '',
                requirements: '',
                responsabilities: '',
                city: '',
                location: '',
                latitude: '',
                country: '',
                longitude: '',
                startDateJobOffer: '',
                endDateJobOffer: '',
                publicationDate: '',
                statusOffer: '',
                company: {
                    idAccount: initialData.idAccount || 0,
                    email: '',
                    website: '',
                    since: '',
                    address: ''
                },
            });
        } catch (error) {
            console.error('Erreur lors de l envoi de la demande au backend:', error);
        }
        window.location.reload()
    };

    //maka ireo liste ana jobOffer nataony
    const [jobOffersList, setJobOffersList] = useState([]);
    const [detailsModalVisible, setDetailsModalVisible] = useState(false);
    const [publishModalVisible, setPublishModalVisible] = useState(false);

    useEffect(() => {
        const fetchJobOffers = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/joboffers/listByAccount/${initialData.idAccount}`);
                console.log('Offres d emploi :', response.data);
                setJobOffersList(response.data);
            } catch (error) {
                if (error.response) {
                    console.error('Réponse du serveur avec code d erreur :', error.response.data);
                } else if (error.request) {
                    console.error('Aucune réponse reçue du serveur :', error.request);
                } else {
                    console.error('Erreur lors de la configuration de la requête :', error.message);
                }
            }            
        };
    
        fetchJobOffers();
    }, [initialData.idAccount]);
    

    const handleDetailsClick = (offer) => {
        setSelectedOffer(offer);
        setDetailsModalVisible(true);
    };

    const handlePublishClick = (offerId) => {
        setSelectedOffer({ id: offerId });
        setPublishModalVisible(true);
    };

// Fonction manokatra ilay modal de mise à jour
const handleEditClick = (offer) => {
    setSelectedOffer(offer);
    setEditModalVisible(true);
};


// Modal mise à jour
const [editModalVisible, setEditModalVisible] = useState(false);
const [selectedOffer, setSelectedOffer] = useState(null);

// Fonction manao mise à jour eto
const handleUpdateOffer = async (updatedOffer) => {
    try {
        const response = await axios.put(`http://localhost:8080/joboffers/updates/${updatedOffer.id}`, updatedOffer);

        console.log('Offre mise à jour avec succès :', response.data);

        // Manao mise à jour ny liste indray isika eto rehefa avy manao mise à jour ireo donnée
        const updatedJobOffers = jobOffersList.map((offer) =>
            offer.id === updatedOffer.id ? response.data : offer
        );
        setJobOffersList(updatedJobOffers);
    } catch (error) {
        console.error('Erreur lors de la mise à jour de l offre :', error);
    }

    // Hidina le modal après la mise à jour na dia misokatra foana aza fa tena modal no saika nataoko teo dia sahirana za niinstalle bootstrap
    setEditModalVisible(false);
};

console.log(selectedOffer)
    return (
        <>
            <div className="wt-admin-right-page-header clearfix">
                <h2>Publier une offre d'emploi</h2>
                <div className="breadcrumbs">
                    <a href="#">Home</a>
                    <a href="#">Tableau de bord</a>
                    <span>Formulaire de soumission d'emploi</span>
                </div>
            </div>

            {/* Basic Information */}
            <div className="panel panel-default">
                <div className="panel-heading wt-panel-heading p-a20">
                    <h4 className="panel-tittle m-a0">
                        <i className="fa fa-suitcase" /> Détails de l'emploi
                    </h4>
                </div>
                <div className="panel-body wt-panel-body p-a20 m-b30 ">
                    <form onSubmit={handleSubmit}>
                        <div className="row">
                            {/* Job title */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Titre du poste.</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="jobTitle"
                                            type="text"
                                            placeholder="Titre de l'emploie"
                                            value={jobOffer.jobTitle}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-address-card" />
                                    </div>
                                </div>
                            </div>
                            {/* Job Category */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group city-outer-bx has-feedback">
                                    <label>Catégorie d'emploi</label>
                                    <div className="ls-inputicon-box">
                                        <select
                                            className="wt-select-box selectpicker"
                                            data-live-search="true"
                                            title=""
                                            id="j-category"
                                            data-bv-field="size"
                                            name="jobCategory"
                                            value={jobOffer.jobCategory}
                                            onChange={handleInputChange}
                                        >
                                            <option disabled value="">
                                                Sélectionner une catégorie
                                            </option>
                                            {jobCategories.map((category) => (
                                                <option key={category} value={category}>
                                                    {category}
                                                </option>
                                            ))}
                                        </select>
                                        <i className="fs-input-icon fa fa-border-all" />
                                    </div>
                                </div>
                            </div>

                            {/* Job Type */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Type d'emploi</label>
                                    <div className="ls-inputicon-box">
                                        <select
                                            className="wt-select-box selectpicker"
                                            data-live-search="true"
                                            title=""
                                            id="s-category"
                                            data-bv-field="size"
                                            name="jobType"
                                            value={jobOffer.jobType}
                                            onChange={handleInputChange}
                                        >
                                            <option className="bs-title-option" value="">
                                                Sélectionner une catégorie
                                            </option>
                                            {jobTypes.map((type) => (
                                                <option key={type} value={type}>
                                                    {type}
                                                </option>
                                            ))}
                                        </select>
                                        <i className="fs-input-icon fa fa-file-alt" />
                                    </div>
                                </div>
                            </div>

                            {/* Offered Salary */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Salaire proposé</label>
                                    <div className="ls-inputicon-box">
                                        <select
                                            className="wt-select-box selectpicker"
                                            data-live-search="true"
                                            title=""
                                            id="salary"
                                            data-bv-field="size"
                                            name="offeredSalary"
                                            value={jobOffer.offeredSalary}
                                            onChange={handleInputChange}
                                        >
                                            <option className="bs-title-option" value="">
                                                Salaire
                                            </option>
                                            {salaryOptions.map((salary) => (
                                                <option key={salary} value={salary}>
                                                    {salary} Ariary
                                                </option>
                                            ))}
                                        </select>
                                        <i className="fs-input-icon fa fa-dollar-sign" />
                                    </div>
                                </div>
                            </div>

                            {/* Experience */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Expérience</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="experience"
                                            type="text"
                                            placeholder="E.g. Minimum 3 years"
                                            value={jobOffer.experience}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-user-edit" />
                                    </div>
                                </div>
                            </div>
                            {/* Qualification */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Qualification</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="qualification"
                                            type="text"
                                            placeholder="Qualification Title"
                                            value={jobOffer.qualification}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-user-graduate" />
                                    </div>
                                </div>
                            </div>
                            {/* Gender */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Genre</label>
                                    <div className="ls-inputicon-box">
                                        <select
                                            className="wt-select-box selectpicker"
                                            data-live-search="true"
                                            title=""
                                            id="gender"
                                            data-bv-field="size"
                                            name="gender"
                                            value={jobOffer.gender}
                                            onChange={handleInputChange}
                                        >
                                            <option className="bs-title-option" value>
                                                Genre
                                            </option>
                                            <option>Masculin</option>
                                            <option>Féminin</option>
                                            <option>Autre</option>
                                        </select>
                                        <i className="fs-input-icon fa fa-venus-mars" />
                                    </div>
                                </div>
                            </div>
                            {/* Country */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Pays</label>
                                    <div className="ls-inputicon-box">
                                        <select
                                            className="wt-select-box selectpicker"
                                            data-live-search="true"
                                            title=""
                                            id="country"
                                            data-bv-field="size"
                                            name="country"
                                            value={jobOffer.country}
                                            onChange={handleInputChange}
                                        >
                                            <option className="bs-title-option" value="">
                                                Pays
                                            </option>
                                            <option value="Madagascar">Madagascar</option>
                                        </select>
                                        <i className="fs-input-icon fa fa-globe-americas" />
                                    </div>
                                </div>
                            </div>

                            {/* City */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>City</label>
                                    <div className="ls-inputicon-box">
                                        <select
                                            className="wt-select-box selectpicker"
                                            data-live-search="true"
                                            title=""
                                            id="city"
                                            data-bv-field="size"
                                            name="city"
                                            value={jobOffer.city}
                                            onChange={handleInputChange}
                                        >
                                            <option className="bs-title-option" value="">
                                                City
                                            </option>
                                            {citiesInMadagascar.map((city) => (
                                                <option key={city} value={city}>
                                                    {city}
                                                </option>
                                            ))}
                                        </select>
                                        <i className="fs-input-icon fa fa-map-marker-alt" />
                                    </div>
                                </div>
                            </div>

                            {/* Location */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Lieu</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="location"
                                            type="text"
                                            placeholder="Type Address"
                                            value={jobOffer.location}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-map-marker-alt" />
                                    </div>
                                </div>
                            </div>
                            {/* Latitude */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Latitude</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="latitude"
                                            type="text"
                                            placeholder="Latitude"
                                            value={jobOffer.latitude}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-map-pin" />
                                    </div>
                                </div>
                            </div>
                            {/* Longitude */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Longitude</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="longitude"
                                            type="text"
                                            placeholder="Longitude"
                                            value={jobOffer.longitude}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-map-pin" />
                                    </div>
                                </div>
                            </div>
                            {/* Requirements */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Requirements</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="requirements"
                                            type="text"
                                            placeholder="Requirements"
                                            value={jobOffer.requirements}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-map-pin" />
                                    </div>
                                </div>
                            </div>
                            {/* Responsabilities */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Responsabilities</label>
                                    <div className="ls-inputicon-box">
                                        <input
                                            className="form-control"
                                            name="responsabilities"
                                            type="text"
                                            placeholder="Responsabilities"
                                            value={jobOffer.responsabilities}
                                            onChange={handleInputChange}
                                        />
                                        <i className="fs-input-icon fa fa-map-pin" />
                                    </div>
                                </div>
                            </div>
                            {/* Description */}
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Description</label>
                                    <textarea
                                        className="form-control"
                                        name="jobDescription"
                                        rows={4}
                                        value={jobOffer.jobDescription}
                                        onChange={handleInputChange}
                                    />
                                </div>
                            </div>
                            {/* Start Date */}
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Date de début</label>
                                    <div className="ls-inputicon-box">
                                        <DatePicker
                                            selected={jobOffer.startDateJobOffer}
                                            onChange={(date) => setJobOffer({ ...jobOffer, startDateJobOffer: date })}
                                            dateFormat="dd-MM-yyyy"
                                            placeholderText="jj-mm-aaaa"
                                        />
                                    </div>
                                </div>
                            </div>
                            {/* End Date */}
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Date de fin</label>
                                    <div className="ls-inputicon-box">
                                        <DatePicker
                                            selected={jobOffer.endDateJobOffer}
                                            onChange={(date) => setJobOffer({ ...jobOffer, endDateJobOffer: date })}
                                            dateFormat="dd-MM-yyyy"
                                            placeholderText="jj-mm-aaaa"
                                        />
                                    </div>
                                </div>
                            </div>
                            {/* StatusOffre */}
                            <div className="col-xl-4 col-lg-6 col-md-12">
                                <div className="form-group">
                                    <label>Status de l Offre</label>
                                    <div className="ls-inputicon-box">
                                        <select
                                            className="wt-select-box selectpicker"
                                            data-live-search="true"
                                            title=""
                                            id="statusOffre"
                                            data-bv-field="size"
                                            name="statusOffer"
                                            value={jobOffer.statusOffer}
                                            onChange={handleInputChange}
                                        >
                                            <option className="bs-title-option" value>
                                                statusOffre
                                            </option>
                                            <option>Enregistrer</option>
                                            <option>Poster</option>
                                            <option>Fermer</option>
                                        </select>
                                        <i className="fs-input-icon fa fa-file-alt" />
                                    </div>
                                </div>
                            </div>

                            {/* Submit Buttons */}
                            <div className="col-lg-12 col-md-12">
                                <div className="text-left">
                                    <button type="submit" className="site-button outline-primary" onClick={handleSubmit}>Enregistrer l' Offre</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            {/* Display Job Offers Table */}
            <div className="panel panel-default">
                <div className="panel-heading wt-panel-heading p-a20">
                    <h4 className="panel-title m-a0">Liste des offres d'emploi</h4>
                </div>
                <div className="panel-body wt-panel-body p-a20 m-b30">
                    <div className="table-responsive">
                        <table className="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Titre du poste</th>
                                    <th>Catégorie</th>
                                    <th>Type</th>
                                    <th>Salaire</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {jobOffersList.map((offer) => (
                                    <tr key={offer.id}>
                                        <td>{offer.jobTitle}</td>
                                        <td>{offer.jobCategory}</td>
                                        <td>{offer.jobType}</td>
                                        <td>{offer.offeredSalary} Ariary</td>
                                        <td>
                                            <button onClick={() => handleDetailsClick(offer)}>Détails</button>
                                            <button onClick={() => handlePublishClick(offer.id)}>Publier</button>
                                            <button onClick={() => handleEditClick(offer)}>Modifier</button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {/* fiantsoana ilay composant Modal de mise à jour */}
            <UpdateJobModal
                show={editModalVisible}
                onHide={() => setEditModalVisible(false)}
                onUpdate={(updatedOffer) => handleUpdateOffer(updatedOffer)}
                offer={selectedOffer}
            />
        </>
    );
}

export default EmpPostAJobPage;