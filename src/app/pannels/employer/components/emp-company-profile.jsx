import { useEffect, useState } from "react";
import JobZImage from "../../../common/jobz-img";
import axios from "axios"

const teamSizeOptions = ["5-10", "10+", "20+", "50+"]
const initialData = JSON.parse(sessionStorage.getItem("employerData"))

function EmpCompanyProfilePage() {

    const [companyName, setCompanyName] = useState("")
    const [phone, setPhone] = useState("")
    const [emailCompany, setEmailCompany] = useState("")
    const [website, setWebsite] = useState("")
    const [since, setSince] = useState("")
    const [teamSize, setTeamSize] = useState("")
    const [companyDescription, setCompanyDescription] = useState("")

    const [photo, setPhoto] = useState()
    const [photoFromApi, setPhotoFromApi] = useState()


    const loadcompanyData = async () => {
        const result = await axios.get("http://localhost:8080/company/list/" + initialData.idAccount)

        setCompanyName(result.data.name)
        setPhone(result.data.phone)
        setEmailCompany(result.data.email)
        setWebsite(result.data.website)
        setSince(result.data.since)
        setTeamSize(result.data.teamSize)
        setCompanyDescription(result.data.descriptionCompany)

        const imageResult = await axios.get("http://localhost:8080/file/download/" + initialData.idAccount)
        setPhotoFromApi(imageResult.config.url)

        console.log("photoFromApi", photoFromApi)

    }

    // 5836a21e - 2c91 - 4bfa - a7c1 - 7b40fac43542.jpg

    useEffect(() => {
        loadcompanyData()
    }, [])

    const updatedCompany = {
        "email": emailCompany,
        "photo": "images/jobs-company/pic4.jpg",
        "phone": phone,
        "name": companyName,
        "website": website,
        "since": since,
        "teamSize": teamSize,
        "descriptionCompany": companyDescription,
    }

    const handleSave = async () => {
        await axios.put("http://localhost:8080/company/update/" + initialData.idAccount, updatedCompany)

        const formData = new FormData()
        formData.append("image", photo)

        if (photo !== undefined) {
            await axios.put("http://localhost:8080/file/upload/" + initialData.idAccount, formData)
        }

        window.location.reload()
    }


    return (
        <>
            <div className="wt-admin-right-page-header clearfix">
                <h2>Profil de l'entreprise</h2>
                <div className="breadcrumbs"><a href="#">Accueil</a><a href="#">Tableau de bord</a><span>Profil de l'entreprise</span></div>
            </div>
            {/*Logo and Cover image*/}
            <div className="panel panel-default">
                <div className="panel-heading wt-panel-heading p-a20">
                    <h4 className="panel-tittle m-a0">Logo</h4>
                </div>
                <div className="panel-body wt-panel-body p-a20 p-b0 m-b30 ">
                    <div className="row">
                        <div className="col-lg-12 col-md-12">
                            <div className="form-group">
                                <div className="dashboard-profile-pic">


                                    <div className="dashboard-profile-photo">
                                        {/* <JobZImage src="images/jobs-company/pic1.jpg" alt="" /> */}
                                        <img src={photoFromApi} alt="profil" />

                                        {/* <img src="/default-company.png" /> */}

                                        <div className="upload-btn-wrapper">
                                            <div id="upload-image-grid" />
                                            <button className="site-button button-sm">Upload Photo</button>
                                            <input onChange={(e) => setPhoto(e.target.files[0])} type="file" name="myfile" id="file-uploader" accept=".jpg, .jpeg, .png" />
                                        </div>
                                    </div>
                                    <p><b>Logo de l'entreprise  : </b> La taille maximale du fichier est de 1MB, la dimension minimale est de 136 x 136 et les fichiers appropriés sont .jpg et .png.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*Basic Information*/}
            <div className="panel panel-default">
                <div className="panel-heading wt-panel-heading p-a20">
                    <h4 className="panel-tittle m-a0">Informations de base</h4>
                </div>
                <div className="panel-body wt-panel-body p-a20 m-b30 ">

                    <div className="row">
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group">
                                <label>Nom de l'entreprise</label>
                                <div className="ls-inputicon-box">
                                    <input value={companyName} onChange={(e) => setCompanyName(e.target.value)} className="form-control" name="company_name" type="text" placeholder="Devid Smith" />
                                    <i className="fs-input-icon fa fa-user " />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group">
                                <label>Téléphone</label>
                                <div className="ls-inputicon-box">
                                    <input value={phone} onChange={(e) => setPhone(e.target.value)} className="form-control" name="company_phone" type="text" placeholder="(251) 1234-456-7890" />
                                    <i className="fs-input-icon fa fa-phone-alt" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group">
                                <label>Adresse électronique</label>
                                <div className="ls-inputicon-box">
                                    <input value={emailCompany} onChange={(e) => setEmailCompany(e.target.value)} className="form-control" name="company_Email" type="email" placeholder="Devid@example.com" />
                                    <i className="fs-input-icon fa fa-envelope" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group">
                                <label>Site web</label>
                                <div className="ls-inputicon-box">
                                    <input value={website} onChange={(e) => setWebsite(e.target.value)} className="form-control" name="company_website" type="text" placeholder="https://..." />
                                    <i className="fs-input-icon fa fa-globe-americas" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group">
                                <label>Est. Depuis le</label>
                                <div className="ls-inputicon-box">
                                    <input value={since} onChange={(e) => setSince(e.target.value)} className="form-control" name="company_since" type="text" placeholder="Since..." />
                                    <i className="fs-input-icon fa fa-globe-americas" />
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-12 col-md-12">
                            <div className="form-group city-outer-bx has-feedback">
                                <label>Taille</label>
                                <div className="ls-inputicon-box">
                                    <select value={teamSize} onChange={(e) => setTeamSize(e.target.value)} className="wt-select-box selectpicker" name="team-size" data-live-search="true" title="team-size" id="city" data-bv-field="size">
                                        {teamSizeOptions.map((option) => (
                                            <option value={option} key={option}>
                                                {option}
                                            </option>
                                        ))}
                                    </select>
                                    <i className="fs-input-icon fa fa-sort-numeric-up" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Description</label>
                                <textarea className="form-control" rows={3} placeholder="Greetings! We are Galaxy Software Development Company." defaultValue={""} value={companyDescription} onChange={(e) => setCompanyDescription(e.target.value)} />
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12">
                            <div className="text-left">
                                <button onClick={handleSave} className="site-button">Save Changes</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )
}

export default EmpCompanyProfilePage;