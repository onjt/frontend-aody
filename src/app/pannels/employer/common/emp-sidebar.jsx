import { useEffect } from "react";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import { loadScript, setMenuActive } from "../../../../globals/constants";
import { empRoute, employer, publicUser } from "../../../../globals/route-names";
import JobZImage from "../../../common/jobz-img";

function EmpSidebarSection(props) {
    const currentpath = useLocation().pathname;
    const navigate = useNavigate();

    const handleLogout = () => {
        sessionStorage.clear();
        navigate(`${publicUser.HOME3}`);
        window.location.reload()
    };

    useEffect(() => {
        loadScript("js/custom.js");
        loadScript("js/emp-sidebar.js");
    })

    return (
        <>
            <nav id="sidebar-admin-wraper" className={props.sidebarActive ? "" : "active"}>
                <div className="page-logo">
                    <NavLink to={publicUser.INITIAL}><JobZImage id="skin_page_logo" src="images/logo-dark.png" alt="" /></NavLink>
                </div>
                <div className="admin-nav scrollbar-macosx">
                    <ul>
                        <li
                            className={setMenuActive(currentpath, empRoute(employer.DASHBOARD))}>
                            <NavLink to={empRoute(employer.DASHBOARD)}><i className="fa fa-home" /><span className="admin-nav-text">Tableau de bord</span></NavLink>
                        </li>
                        <li
                            className={setMenuActive(currentpath, empRoute(employer.PROFILE))}>
                            <NavLink to={empRoute(employer.PROFILE)}><i className="fa fa-user-tie" /><span className="admin-nav-text">Profil de l'entreprise</span></NavLink>
                        </li>
                        <li
                            className={
                                setMenuActive(currentpath, empRoute(employer.POST_A_JOB)) +
                                setMenuActive(currentpath, empRoute(employer.MANAGE_JOBS))
                            }>
                            <a href="#">
                                <i className="fa fa-suitcase" />
                                <span className="admin-nav-text">Offres</span>
                            </a>
                            <ul className="sub-menu">
                                <li> <NavLink to={empRoute(employer.POST_A_JOB)} id="jobMenuId1"><span className="admin-nav-text">Nouvelle offre</span></NavLink></li>
                                <li> <NavLink to={empRoute(employer.MANAGE_JOBS)} id="jobMenuId2"><span className="admin-nav-text">Offres postées</span></NavLink></li>
                            </ul>
                        </li>
                        <li className={setMenuActive(currentpath, empRoute(employer.CANDIDATES))}>
                            <NavLink to={empRoute(employer.CANDIDATES)}><i className="fa fa-user-friends" /><span className="admin-nav-text">Candidats</span></NavLink>
                        </li>
                        
                        <li className={setMenuActive(currentpath, empRoute(employer.PACKAGES))}>
                            <NavLink to={empRoute(employer.PACKAGES)}><i className="fa fa-money-bill-alt" /><span className="admin-nav-text">Forfaits</span></NavLink>
                        </li>

                        <li className={setMenuActive(currentpath, empRoute(employer.CHANGE_PASSWORD))}>
                            <NavLink to={empRoute(employer.CHANGE_PASSWORD)}><i className="fa fa-fingerprint" /><span className="admin-nav-text">Changement de mot de passe</span></NavLink></li>

                        <li>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#logout-dash-profile">
                                <i className="fa fa-share-square" />
                                <span className="admin-nav-text" onClick={handleLogout}>Déconnexion</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default EmpSidebarSection;