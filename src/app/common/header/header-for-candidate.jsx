import JobZImage from "../jobz-img";
import { NavLink } from "react-router-dom";
import { candidate, publicUser,canRoute } from "../../../globals/route-names";
import { useState } from "react";

function HeaderForCandidate({ _config }) {

    const [menuActive, setMenuActive] = useState(false);

    function handleNavigationClick() {
        setMenuActive(!menuActive);
    }

    return (
        <>
            <header className={"site-header " + _config.style + " mobile-sider-drawer-menu " + (menuActive ? "active" : "") }>
                <div className="sticky-header main-bar-wraper navbar-expand-lg">
                    <div className="main-bar">
                        <div className="container-fluid clearfix">
                            <div className="logo-header">
                                <div className="logo-header-inner logo-header-one">
                                    <NavLink to={publicUser.INITIAL}>
                                        {
                                            _config.withBlackLogo
                                                ?
                                                <JobZImage src="images/logo-12.png" alt="" />
                                                :
                                                (
                                                    _config.withWhiteLogo
                                                        ?
                                                        <JobZImage src="images/logo-12.png" alt="" />
                                                        :
                                                        (
                                                            _config.withLightLogo ?
                                                                <>
                                                                    <JobZImage id="skin_header_logo_light" src="images/logo-12.png" alt="" className="default-scroll-show" />
                                                                    <JobZImage id="skin_header_logo" src="images/logo-12.png" alt="" className="on-scroll-show" />
                                                                </> :
                                                                <JobZImage id="skin_header_logo" src="images/logo-12.png" alt="" />
                                                        )
                                                )
                                        }
                                    </NavLink>
                                </div>
                            </div>
                            {/* NAV Toggle Button */}
                            <button id="mobile-side-drawer"
                                data-target=".header-nav"
                                data-toggle="collapse"
                                type="button"
                                className="navbar-toggler collapsed"
                                onClick={handleNavigationClick}
                            >
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar icon-bar-first" />
                                <span className="icon-bar icon-bar-two" />
                                <span className="icon-bar icon-bar-three" />
                            </button>
                            {/* MAIN Vav */}
                            <div className="nav-animation header-nav navbar-collapse collapse d-flex justify-content-center">
                                <ul className=" nav navbar-nav">
                                    <li className="has-mega-menu"><NavLink to={publicUser.INITIAL}>Accueil</NavLink>   
                                    </li>
                                    <li className="has-child"><NavLink to={publicUser.jobs.LIST}>Offres</NavLink>   
                                    </li>
                                    <li className="has-child"><NavLink to={publicUser.employer.GRID}>Entreprises</NavLink>
                                    </li>
                                    <li className="has-child"><NavLink to={publicUser.pages.ABOUT}>Pages</NavLink>
                                         <ul className="sub-menu">
                                            <li><NavLink to={publicUser.pages.ABOUT}>À propos</NavLink></li>
                                            <li><NavLink to={publicUser.pages.CONTACT}>Contact</NavLink></li>
                                            <li><NavLink to={publicUser.pages.LOGIN}>Connexion</NavLink></li>
                                            
                                        </ul> 
                                    </li>
                                       
                                    <li className="has-child"><NavLink to={publicUser.pages.PRICING}>Forfait</NavLink>
                                    </li>
                                    
                                    
                                </ul>
                            </div>
                            {/* Header Right Section*/}
                             
                            <div className="header-right">
                                <ul className="header-widget-wrap">
                                    <li className="header-widget">
                                        <div className="extra-nav header-2-nav">
                                            <div className="dashboard-user-section">
                                                <div className="listing-user">
                                                    <div className="dropdown">
                                                        <a href="#" className="dropdown-toggle" id="ID-ACCOUNT_dropdown" data-bs-toggle="dropdown">
                                                            <div className="user-name text-black">
                                                                <span>
                                                                    <JobZImage src="images/user-avtar/pic4.jpg" alt="" />
                                                                </span>
                                                            </div>
                                                        </a>
                                                        <div className="dropdown-menu" aria-labelledby="ID-ACCOUNT_dropdown">
                                                            <ul>
                                                                <li><NavLink to={canRoute(candidate.PROFILE)}><i className="fa fa-user" /> Profil</NavLink></li>
                                                                <li><a href="#" data-bs-toggle="modal" data-bs-target="#logout-dash-profile"><i className="fa fa-share-square" />Déconnexion</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    {/* SITE Search */}
                    <div id="search">
                        <span className="close" />
                        <form role="search" id="searchform" action="/search" method="get" className="radius-xl">
                            <input className="form-control" name="q" type="search" placeholder="Type to search" />
                            <span className="input-group-append">
                                <button type="button" className="search-btn">
                                    <i className="fa fa-paper-plane" />
                                </button>
                            </span>
                        </form>
                    </div>
                </div>
            </header>
            
        </>
    )
}

export default HeaderForCandidate;