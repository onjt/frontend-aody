import axios from "axios"

function StudentViewPopup({ selectedStudent }) {

    const handleClick = async (event) => {
        event.preventDefault()

        if (event.target.name === "desactivate") {
            await axios.put("http://localhost:8080/student/update/" + selectedStudent.idAccount, { "statusAccount": "Désactivé" })


        } else {
            await axios.put("http://localhost:8080/student/update/" + selectedStudent.idAccount, { "statusAccount": "Activé" })
        }

        window.location.reload()

    }

    return (
        <>
            <div className="modal fade twm-saved-jobs-view" id="student-view" aria-hidden="true" aria-labelledby="sign_up_popupLabel-3" tabIndex={-1}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h2 className="modal-title" id="sign_up_popupLabel-3">Details</h2>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
                            </div>
                            <div className="modal-body">
                                <ul className="list-unstyled">
                                    <li><strong>{selectedStudent.name} - {selectedStudent.jobCategory} </strong>
                                        <p> {selectedStudent.email} <br /> {selectedStudent.phone} <br /> {selectedStudent.fullAddress} {selectedStudent.city} {selectedStudent.country} </p>
                                    </li>
                                    <li><strong>Experience</strong><p>{selectedStudent.experience}</p></li>
                                    <li><strong>Description</strong>
                                        <p>{selectedStudent.descriptionStudent}.</p>
                                    </li>
                                    <li><strong>Status : </strong>{selectedStudent.statusAccount}</li>
                                </ul>
                            </div>
                            <div className="modal-footer">
                                {
                                    (selectedStudent.statusAccount === "Activé") &&
                                    <button onClick={handleClick} type="submit" name="desactivate" className="site-button" data-bs-dismiss="modal">Desactivé</button>
                                }
                                {
                                    (selectedStudent.statusAccount === "Désactivé") &&
                                    <button onClick={handleClick} type="submit" name="activate" className="site-button" data-bs-dismiss="modal">Activé</button>
                                }
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}
export default StudentViewPopup