import axios from "axios"


function CompanyViewPopup({ selectedCompany }) {


    const handleClick = async (event) => {
        event.preventDefault()

        if (event.target.name === "desactivate") {
            await axios.put("http://localhost:8080/company/validate/" + selectedCompany.idAccount, { "statusAccount": "Désactivé" })


        } else {
            await axios.put("http://localhost:8080/company/validate/" + selectedCompany.idAccount, { "statusAccount": "Activé" })
        }

        window.location.reload()

    }

    return (
        <>
            <div className="modal fade twm-saved-jobs-view" id="company-view" aria-hidden="true" aria-labelledby="sign_up_popupLabel-3" tabIndex={-1}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h2 className="modal-title" id="sign_up_popupLabel-3">Details</h2>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
                            </div>
                            <div className="modal-body">
                                <ul className="list-unstyled">
                                    <li><strong>{selectedCompany.name}</strong>
                                        <p> {selectedCompany.email} <br /> {selectedCompany.phone} <br /> {selectedCompany.headquarter}</p>
                                    </li>
                                    <li><strong>Site web</strong><p>{selectedCompany.website}</p></li>
                                    <li><strong>Description</strong>
                                        <p>{selectedCompany.descriptionCompany}.</p>
                                    </li>
                                    <li><strong>Status : </strong>{selectedCompany.statusAccount}</li>
                                </ul>
                            </div>
                            <div className="modal-footer">
                                {
                                    (selectedCompany.statusAccount === "Activé") &&
                                    <button onClick={handleClick} type="submit" name="desactivate" className="site-button" data-bs-dismiss="modal">Desactivé</button>
                                }
                                {
                                    (selectedCompany.statusAccount === "Désactivé") &&
                                    <button onClick={handleClick} type="submit" name="activate" className="site-button" data-bs-dismiss="modal">Activé</button>
                                }
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}
export default CompanyViewPopup