import { useEffect, useState } from "react";
import axios from "axios"

function SignUpPopup() {

    // student
    const [usernameStudent, setUsernameStudent] = useState("")
    const [emailStudent, setEmailStudent] = useState("")
    const [passwordStudent, setPasswordStudent] = useState("")
    const [confirmPasswordStudent, setConfirmPasswordStudent] = useState("")
    const [acceptedTerms, setAcceptedTerms] = useState(false)


    // company
    const [usernameCompany, setUsernameCompany] = useState("")
    const [emailCompany, setEmailCompany] = useState("")
    const [passwordCompany, setPasswordCompany] = useState("")
    const [confirmPasswordCompany, setConfirmPasswordCompany] = useState("")
    const [acceptedTermsCompany, setAcceptedTermsCompany] = useState(false)


    const [message, setMessage] = useState("")

    const studentData = {
        "email": emailStudent,
        "password": passwordStudent,
        "photo": "images/b730c168-555c-443f-bf99-c26f0a73e913.png",
        "phone": "000 00 000 00",
        "statusAccount": "Activé",
        "creationDate": new Date().toLocaleString(),
        "name": usernameStudent,
        "qualification": "default",
        "language": "Malagasy",
        "jobCategory": "Stagiaire en ...",
        "experience": "Deux ans d'experience",
        "currentSalary": 0.0,
        "expectedSalary": 0.0,
        "dateOfBirth": "01/01/2000",
        "country": "Madagascar",
        "city": "Antananarivo",
        "postcode": 101,
        "fullAddress": "XYZ 132 Andringitra",
        "descriptionStudent": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus esse placeat accusantium eveniet itaque unde libero saepe.",
        "gender": "default",
    }

    const handleStudentPost = async (event) => {

        event.preventDefault()

        if (passwordStudent === confirmPasswordStudent) {
            if (acceptedTerms) {
                await axios.post("http://localhost:8080/student/save", studentData)

                setMessage("Inscription réussie")

                setUsernameStudent("")
                setEmailStudent("")
                setPasswordStudent("")
                setConfirmPasswordStudent("")
                setAcceptedTerms(false)

            } else {
                setMessage("Veuillez accepter les termes et conditions")
            }
        } else {
            setMessage("Veuillez verifier votre mot de passe")
        }

    }



    const companyData = {
        "email": emailCompany,
        "password": passwordCompany,
        "photo": "images/b730c168-555c-443f-bf99-c26f0a73e913.png",
        "phone": "000 00 000 00",
        "statusAccount": "Activé",
        "creationDate": new Date().toLocaleString(),
        "name": usernameCompany,
        "website": "www.test.mg",
        "since": "1900",
        "teamSize": "XYZ Antananarivo 123",
        "descriptionCompany": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus esse placeat accusantium eveniet itaque unde libero saepe.",
        "headquarter": "Soalandy city"
    }

    const handleCompanyPost = async (event) => {

        event.preventDefault()

        if (passwordCompany === confirmPasswordCompany) {
            if (acceptedTermsCompany) {
                await axios.post("http://localhost:8080/company/save", companyData)

                setMessage("Inscription réussie")

                setUsernameCompany("")
                setEmailCompany("")
                setPasswordCompany("")
                setConfirmPasswordCompany("")
                setAcceptedTermsCompany(false)

            } else {
                setMessage("Veuillez accepter les termes et conditions")
            }
        } else {
            setMessage("Veuillez verifier votre mot de passe")
        }
    }

    return (
        <>
            <div className="modal fade twm-sign-up" id="sign_up_popup" aria-hidden="true" aria-labelledby="sign_up_popupLabel" tabIndex={-1}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h2 className="modal-title" id="sign_up_popupLabel">S'inscrire</h2>
                                <p>Inscrivez-vous et accédez à toutes les fonctionnalités de Aody</p>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
                            </div>
                            <div className="modal-body">
                                <div className="twm-tabs-style-2">
                                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                                        {/*Signup Candidate*/}
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#sign-candidate" type="button"><i className="fas fa-user-tie" />Etudiant</button>
                                        </li>
                                        {/*Signup Employer*/}
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link" data-bs-toggle="tab" data-bs-target="#sign-Employer" type="button"><i className="fas fa-building" />Entreprise</button>
                                        </li>
                                    </ul>
                                    <div className="tab-content" id="myTabContent">




                                        {/*Signup Candidate Content*/}
                                        <div className="tab-pane fade show active" id="sign-candidate">
                                            <div className="row">

                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={usernameStudent} onChange={(event) => setUsernameStudent(event.target.value)} name="username" type="text" required className="form-control" placeholder="Nom d'utilisateur *" />
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={emailStudent} onChange={(event) => setEmailStudent(event.target.value)} name="email" type="email" className="form-control" required placeholder="Adresse mail *" />
                                                    </div>
                                                </div>


                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={passwordStudent} onChange={(event) => setPasswordStudent(event.target.value)} name="password" type="password" className="form-control" required placeholder="Mot de passe *" />
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={confirmPasswordStudent} onChange={(event) => setConfirmPasswordStudent(event.target.value)} name="confirmPassword" type="password" className="form-control" required placeholder="Confirmer mot de passe *" />
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <div className=" form-check">
                                                            <input checked={acceptedTerms} onChange={(event) => setAcceptedTerms(event.target.checked)} name="isAcceptedTerms" type="checkbox" className="form-check-input" id="agree1" />
                                                            <label className="form-check-label" htmlFor="agree1">J'accepte  <a href="#">les termes et conditions</a></label>
                                                            <p>Déjà inscrit ?
                                                                <button className="twm-backto-login" data-bs-target="#sign_up_popup2" data-bs-toggle="modal" data-bs-dismiss="modal">Connectez-vous ici</button>
                                                            </p>
                                                            <p>
                                                                {message}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <button type="submit" onClick={handleStudentPost} className="site-button">S'inscrire</button>
                                                </div>
                                            </div>
                                        </div>


                                        {/*Signup Employer Content*/}
                                        <div className="tab-pane fade" id="sign-Employer">
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={usernameCompany} onChange={(event) => setUsernameCompany(event.target.value)} name="usernameCompany" type="text" required className="form-control" placeholder="Nom d'utilisateur *" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={emailCompany} onChange={(event) => setEmailCompany(event.target.value)} name="emailCompany" type="text" className="form-control" required placeholder="Adresse mail *" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={passwordCompany} onChange={(event => setPasswordCompany(event.target.value))} name="passwordCompany" type="password" className="form-control" required placeholder="Mot de passe *" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <input value={confirmPasswordCompany} onChange={(event) => setConfirmPasswordCompany(event.target.value)} name="confirmPassword" type="password" className="form-control" required placeholder="Confirmer mot de passe *" />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <div className="form-group mb-3">
                                                        <div className=" form-check">
                                                            <input checked={acceptedTermsCompany} onChange={(event) => setAcceptedTermsCompany(event.target.checked)} type="checkbox" className="form-check-input" id="agree2" />
                                                            <label className="form-check-label" htmlFor="agree2">J'accepte <a href="#">les termes et conditions</a></label>
                                                            <p>Déjà inscrit ?
                                                                <button className="twm-backto-login" data-bs-target="#sign_up_popup2" data-bs-toggle="modal" data-bs-dismiss="modal">Connectez-vous ici</button>
                                                            </p>
                                                            <p>
                                                                {message}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <button onClick={handleCompanyPost} type="submit" className="site-button">S'inscrire</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <span className="modal-f-title">Se connecter ou s'inscrire avec</span>
                                <ul className="twm-modal-social">
                                    <li><a href="https://www.facebook.com/" className="facebook-clr"><i className="fab fa-facebook-f" /></a></li>
                                    <li><a href="https://www.twitter.com/" className="twitter-clr"><i className="fab fa-twitter" /></a></li>
                                    <li><a href="https://in.linkedin.com/" className="linkedin-clr"><i className="fab fa-linkedin-in" /></a></li>
                                    <li><a href="https://www.google.com/signup" className="google-clr"><i className="fab fa-google" /></a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </>
    )
}

export default SignUpPopup;