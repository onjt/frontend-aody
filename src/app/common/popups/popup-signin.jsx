import axios from "axios";
import { useEffect, useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { useNavigate } from "react-router-dom";
import { admin, adminRoute, canRoute, candidate, empRoute, employer } from "../../../globals/route-names";

function SignInPopup() {
    const navigate = useNavigate();
    const [canMail, setCanMail] = useState("");
    const [empMail, setEmpMail] = useState("");
    const [canPassword, setCanPassword] = useState("");
    const [empPassword, setEmpPassword] = useState("");
    const [rememberMe, setRememberMe] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [recaptcha, setRecaptcha] = useState(false);

    const onChange = () => {
        setRecaptcha(true);
    };

    const handleAdminLogin = (event) => {
        event.preventDefault();
        loginAdmin();
    };

    const handleCandidateLogin = async (event) => {
        event.preventDefault();
        await handleLogin('/login/loginStudent', { email: canMail, password: canPassword }, moveToCandidate);
    };

    const handleEmployerLogin = async (event) => {
        event.preventDefault();
        await handleLogin('/login/loginCompany', { email: empMail, password: empPassword }, moveToEmployer);
    };
    
    const nomAdmin = "aody@gmail.com";
    const motDePasse = "tahiry";
    const [adminUsername, setAdminUsername] = useState(nomAdmin);
    const [password, setPassword] = useState(motDePasse);


    const loginAdmin = () => {
        // Vérifiez si les données saisies sont égales à "tahiry"
        if (adminUsername === "aody@gmail.com" && password === "tahiry") {
        console.log("Admin autorisé");
        // Redirigez vers le tableau de bord de l'admin
        navigate(adminRoute(admin.DASHBOARD));
        } else {
        console.log("Admin non autorisé");
        }
    };
    
    

    const handleLogin = async (endpoint, data, successCallback) => {
        if (recaptcha) {
        try {
            const response = await axios.post(`http://localhost:8080${endpoint}`, data);
            handleLoginResponse(response, successCallback);
        } catch (error) {
            console.error(`An error occurred during ${endpoint} login:`, error);
            if (error.response && error.response.data) {
            setErrorMessage(error.response.data);
            } else {
            setErrorMessage("Votre email ou mot de passe est incorrecte veuillez vous identifier ou créer un compte");
            }
        }
        }
        setRecaptcha(false);
    };

    const handleLoginResponse = (response, successCallback) => {
        try {
        console.log("Response received:", response);

        if (response.data) {
            console.log("Received object response:", response.data);

            const commonFields = ['idAccount', 'email', 'password', 'photo', 'phone', 'name'];
            const specificFields = response.data.hasOwnProperty('descriptionStudent') ? [
            'qualification', 'language', 'jobCategory', 'experience', 'currentSalary',
            'expectedSalary', 'dateOfBirth', 'country', 'city', 'postcode', 'fullAddress',
            'descriptionStudent', 'gender'
            ] : [
            'website', 'since', 'teamSize', 'descriptionCompany', 'headquarter'
            ];

            const userData = Object.fromEntries(commonFields.concat(specificFields).map(field => [field, response.data[field]]));
            const userKey = response.data.hasOwnProperty('descriptionStudent') ? 'candidateData' : 'employerData';

            sessionStorage.setItem(userKey, JSON.stringify(userData));
            successCallback();
            closePopup();
        } else {
            console.error("Invalid response structure:", response);
            setErrorMessage("Une erreur inattendue s'est produite");
        }
        } catch (error) {
        console.error("Error handling login response:", error);
        setErrorMessage("Une erreur inattendue s'est produite");
        }
    };

    const closePopup = () => {
        const modal = document.getElementById("sign_up_popup2");
        const backdrop = document.getElementsByClassName("modal-backdrop")[0];
        modal.classList.remove("show");
        backdrop.remove();
        window.location.reload();
    };

    useEffect(() => {
        const storedCanUser = localStorage.getItem("canUser");
        const storedEmpUser = localStorage.getItem("empUser");

        if (storedCanUser) {
        const { mail, password } = JSON.parse(storedCanUser);
        setCanMail(mail);
        setCanPassword(password);
        }

        if (storedEmpUser) {
        const { mail, password } = JSON.parse(storedEmpUser);
        setEmpMail(mail);
        setEmpPassword(password);
        }
    }, []);

        const moveToCandidate = () => {
            //Alaintsika indray ny donner ana session
            const userData = JSON.parse(sessionStorage.getItem('candidateData'));
            console.log("Candidate ID:", userData.idAccount);
            console.log("Candidate Email:", userData.email);
            navigate(canRoute(candidate.DASHBOARD));
        };

    const moveToEmployer = () => {
        const userData = JSON.parse(sessionStorage.getItem('employerData'));
        console.log("Employer ID:", userData.idAccount);
        console.log("Employer Email:", userData.email);
        navigate(empRoute(employer.DASHBOARD));
    };


    return (
        <>
            <div className="modal fade twm-sign-up" id="sign_up_popup2" aria-hidden="true" aria-labelledby="sign_up_popupLabel2" tabIndex={-1}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h2 className="modal-title" id="sign_up_popupLabel2">Se connecter</h2>
                            <p>Connectez-vous pour accéder à toutes les fonctionnalités de Aody</p>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
                        </div>
                        <div className="modal-body">
                            <div className="twm-tabs-style-2">
                                <ul className="nav nav-tabs" id="myTab2" role="tablist">
                                    {/*Login Candidate*/}
                                    <li className="nav-item">
                                        <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#login-candidate" type="button"><i className="fas fa-user-tie" />Etudiant</button>
                                    </li>
                                    {/*Login Employer*/}
                                    <li className="nav-item">
                                        <button className="nav-link" data-bs-toggle="tab" data-bs-target="#login-Employer" type="button"><i className="fas fa-building" />Entreprise</button>
                                    </li>
                                    <li className="nav-item">
                                        <button className="nav-link" data-bs-toggle="tab" data-bs-target="#login-admin" type="button"><i className="fas fa-door-open" />Administrateur</button>
                                    </li>
                                </ul>
                                <div className="tab-content" id="myTab2Content">
                                    {/*Login Candidate Content*/}
                                    <form onSubmit={handleCandidateLogin} className="tab-pane fade show active" id="login-candidate">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="mail"
                                                        type="text"
                                                        required
                                                        className="form-control"
                                                        placeholder="Adresse e-mail"
                                                        value={canMail}
                                                        onChange={(event) => {
                                                            setCanMail(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="password"
                                                        type="password"
                                                        className="form-control"
                                                        required
                                                        placeholder="Mot de passe"
                                                        value={canPassword}
                                                        onChange={(event) => {
                                                            setCanPassword(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <div className=" form-check">
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            id="RememberMeCandidate"
                                                            checked={rememberMe}
                                                            onChange={() => setRememberMe(!rememberMe)}
                                                        />
                                                        <label className="form-check-label rem-forgot" htmlFor="RememberMe">
                                                            Se souvenir de moi <a href="#">Mot de passe oublié</a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <ReCAPTCHA
                                                sitekey="6Le4uEYpAAAAAOARDDh8R_JlDzKTeDA_gmQjLFox"
                                                onChange={onChange}
                                                />
                                            {errorMessage && <p>{errorMessage}</p>}
                                            <div className="col-md-12">
                                                <button type="submit" className="site-button">
                                                    Se connecter
                                                </button>
                                                <div className="mt-3 mb-3">
                                                    Vous n'avez pas de compte ?
                                                    <button
                                                        className="twm-backto-login"
                                                        data-bs-target="#sign_up_popup"
                                                        data-bs-toggle="modal"
                                                        data-bs-dismiss="modal"
                                                    >
                                                        Inscription
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    {/*Login Employer Content*/}
                                    <form onSubmit={handleEmployerLogin} className="tab-pane fade" id="login-Employer">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="mail"
                                                        type="text"
                                                        required
                                                        className="form-control"
                                                        placeholder="Adresse e-mail*"
                                                        value={empMail}
                                                        onChange={(event) => {
                                                            setEmpMail(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="password"
                                                        type="password"
                                                        className="form-control"
                                                        required
                                                        placeholder="Mot de passe*"
                                                        value={empPassword}
                                                        onChange={(event) => {
                                                            setEmpPassword(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <div className=" form-check">
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            id="RememberMeEmployer"
                                                            checked={rememberMe}
                                                            onChange={() => setRememberMe(!rememberMe)}
                                                        />
                                                        <label className="form-check-label rem-forgot" htmlFor="RememberMe">
                                                            Se souvenir de moi <a href="#">Mot de passe oublié</a>
                                                        </label>
                                                        <ReCAPTCHA
                                                            sitekey="6Le4uEYpAAAAAOARDDh8R_JlDzKTeDA_gmQjLFox"
                                                            onChange={onChange}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            {errorMessage && <p>{errorMessage}</p>}
                                            <div className="col-md-12">
                                                <button type="submit" className="site-button">
                                                    Se connecter
                                                </button>
                                                <div className="mt-3 mb-3">
                                                    Vous n'avez pas de compte ?
                                                    <button
                                                        className="twm-backto-login"
                                                        data-bs-target="#sign_up_popup"
                                                        data-bs-toggle="modal"
                                                        data-bs-dismiss="modal"
                                                    >
                                                        Inscription
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    {/*Login Admin Content*/}
                                    <form onSubmit={handleAdminLogin} className="tab-pane fade" id="login-admin">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="username"
                                                        type="text"
                                                        required
                                                        className="form-control"
                                                        placeholder="Username*"
                                                        value={adminUsername}
                                                        onChange={(event) => {
                                                            setAdminUsername(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="password"
                                                        type="password"
                                                        className="form-control"
                                                        required
                                                        placeholder="Password*"
                                                        value={password}
                                                        onChange={(event) => {
                                                            setPassword(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <div className=" form-check">
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            id="Password3"
                                                            checked={rememberMe}
                                                            onChange={() => setRememberMe(!rememberMe)}
                                                        />
                                                        <label className="form-check-label rem-forgot" htmlFor="Password3">
                                                            Se souvenir de moi <a href="#">Mot de passe oublié</a>
                                                        </label>
                                                        <ReCAPTCHA
                                                            sitekey="6Le4uEYpAAAAAOARDDh8R_JlDzKTeDA_gmQjLFox"
                                                            onChange={onChange}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            {errorMessage && <p>{errorMessage}</p>}
                                            <div className="col-md-12">
                                                <button type="submit" className="site-button">
                                                    Se connecter
                                                </button>
                                                <div className="mt-3 mb-3">
                                                    Vous n'avez pas de compte ?
                                                    <button
                                                        className="twm-backto-login"
                                                        data-bs-target="#sign_up_popup"
                                                        data-bs-toggle="modal"
                                                        data-bs-dismiss="modal"
                                                    >
                                                        Inscription
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <form onSubmit={handleEmployerLogin} className="tab-pane fade" id="login-admin">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="mail"
                                                        type="text"
                                                        required
                                                        className="form-control"
                                                        placeholder="Adresse e-mail*"
                                                        value={empMail}
                                                        onChange={(event) => {
                                                            setEmpMail(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <input
                                                        name="password"
                                                        type="password"
                                                        className="form-control"
                                                        required
                                                        placeholder="Mot de passe*"
                                                        value={empPassword}
                                                        onChange={(event) => {
                                                            setEmpPassword(event.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group mb-3">
                                                    <div className=" form-check">
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            id="RememberMeEmployer"
                                                            checked={rememberMe}
                                                            onChange={() => setRememberMe(!rememberMe)}
                                                        />
                                                        <label className="form-check-label rem-forgot" htmlFor="RememberMe">
                                                            Se souvenir de moi <a href="#">Mot de passe oublié</a>
                                                        </label>
                                                        <ReCAPTCHA
                                                            sitekey="6Le4uEYpAAAAAOARDDh8R_JlDzKTeDA_gmQjLFox"
                                                            onChange={onChange}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            {errorMessage && <p>{errorMessage}</p>}
                                            <div className="col-md-12">
                                                <button type="submit" className="site-button">
                                                    Se connecter
                                                </button>
                                                <div className="mt-3 mb-3">
                                                    Vous n'avez pas de compte ?
                                                    <button
                                                        className="twm-backto-login"
                                                        data-bs-target="#sign_up_popup"
                                                        data-bs-toggle="modal"
                                                        data-bs-dismiss="modal"
                                                    >
                                                        Inscription
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <span className="modal-f-title">Se connecter ou Créer Compte avec</span>
                            <ul className="twm-modal-social">
                                <li>
                                    <a href="https://www.facebook.com/" className="facebook-clr">
                                        <i className="fab fa-facebook-f" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.twitter.com/" className="twitter-clr">
                                        <i className="fab fa-twitter" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://in.linkedin.com/" className="linkedin-clr">
                                        <i className="fab fa-linkedin-in" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.google.com/" className="google-clr">
                                        <i className="fab fa-google" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default SignInPopup;
