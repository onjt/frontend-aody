import { publicUrlFor } from "../../../globals/constants";
import JobZImage from "../jobz-img";
import { NavLink } from "react-router-dom";
import { publicUser } from "../../../globals/route-names";

function Footer1() {
    return (
        <>
            <footer className="footer-dark" style={{backgroundColor: "#135B67"}}>
                <div className="container">
                    {/* FOOTER BLOCKES START */}
                    <div className="footer-top">
                        <div className="row">
                            <div className="col-lg-3 col-md-12">
                                <div className="widget widget_about">
                                    <div className="logo-footer clearfix">
                                        <NavLink to={publicUser.HOME3}><JobZImage id="skin_footer_dark_logo" src="images/logo-12.png" alt="" /></NavLink>
                                    </div>
                                    <p>Une plateforme de mise en relation entre entreprises et étudiants</p>
                                    <ul className="ftr-list">
                                        <li><p><span>Address :</span>Lot II 62 B Faravohitra Antananarivo, 101</p></li>
                                        <li><p><span>Email :</span>aody.inclusiv@gmail.com</p></li>
                                        <li><p><span>Téléphone :</span>032 97 875 77</p></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-9 col-md-12">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-6">
                                        <div className="widget widget_services ftr-list-center">
                                            <h3 className="widget-title">Plan du site</h3>
                                            <ul>
                                                <li><NavLink to={publicUser.INITIAL}>Accueil</NavLink></li>
                                                <li><NavLink to={publicUser.jobs.GRID}>Offres</NavLink></li>
                                                <li><NavLink to={publicUser.jobs.LIST}>Entreprises</NavLink></li>
                                                <li><NavLink to={publicUser.pages.PRICING}>Nos forfaits</NavLink></li>
                                                <li><NavLink to={publicUser.candidate.LIST}>Candidats</NavLink></li>
                                            </ul>
                                        </div>
                                    </div>
                                    {/* <div className="col-lg-3 col-md-6 col-sm-6">
                                        <div className="widget widget_services ftr-list-center">
                                            <h3 className="widget-title">For Employers</h3>
                                            <ul>
                                                <li><NavLink to={publicUser.blog.GRID1}>Blog Grid</NavLink></li>
                                                <li><NavLink to={publicUser.pages.CONTACT}>Contact</NavLink></li>
                                                <li><NavLink to={publicUser.jobs.LIST}>Jobs Listing</NavLink></li>
                                                <li><NavLink to={publicUser.jobs.DETAIL1}>Jobs details</NavLink></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-6">
                                        <div className="widget widget_services ftr-list-center">
                                            <h3 className="widget-title">Helpful Resources</h3>
                                            <ul>
                                                <li><NavLink to={publicUser.pages.FAQ}>FAQs</NavLink></li>
                                                <li><NavLink to={publicUser.pages.LOGIN}>Profile</NavLink></li>
                                                <li><NavLink to={publicUser.pages.ERROR404}>404 Page</NavLink></li>
                                                <li><NavLink to={publicUser.pages.PRICING}>Pricing</NavLink></li>
                                            </ul>
                                        </div>
                                    </div> */}
                                    <div className="col-lg-3 col-md-6 col-sm-6">
                                        <div className="widget widget_services ftr-list-center">
                                            <h3 className="widget-title">Liens rapides</h3>
                                            <ul>
                                                <li><NavLink to={publicUser.pages.CONTACT}>Contactez-nous</NavLink></li>
                                                <li><NavLink to={publicUser.pages.ABOUT}>À propos</NavLink></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* FOOTER COPYRIGHT */}
                    <div className="footer-bottom">
                        <div className="footer-bottom-info">
                            <div className="footer-copy-right">
                                <span className="copyrights-text">Copyright © 2024 by AODY SARL All Rights Reserved.</span>
                            </div>
                            <ul className="social-icons">
                                <li><a href="https://www.facebook.com/" className="fab fa-facebook-f" /></li>
                                <li><a href="https://www.twitter.com/" className="fab fa-twitter" /></li>
                                <li><a href="https://www.instagram.com/" className="fab fa-instagram" /></li>
                                <li><a href="https://www.youtube.com/" className="fab fa-youtube" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>

        </>
    )
}

export default Footer1;