import axios from "axios"

const url = "http://localhost:8080/student";

export const addStudent = (data) => {
    return (dispatch) => {
        return axios
            .post(`${url}/save`, data)
            .then((res) => {
                if (res.data.errors) {
                    dispatch("error")
                } else {
                    dispatch("success")
                }
            });
    };
};

