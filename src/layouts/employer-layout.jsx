import { useEffect, useState } from "react";
import YesNoPopup from "../app/common/popups/popup-yes-no";
import EmpHeaderSection from "../app/pannels/employer/common/emp-header";
import EmpSidebarSection from "../app/pannels/employer/common/emp-sidebar";
import { popupType } from "../globals/constants";
import EmployerRoutes from "../routing/employer-routes";

function EmployerLayout() {

    useEffect(() => {
        // Vérifier la présence de la clé dans sessionStorage
        const hasSessionData = sessionStorage.getItem('employerData');
    
        // Si la clé n'est pas présente, rediriger vers la page de connexion
        if (!hasSessionData) {
            window.location.href = '/login';
        }
    }, []);
    const [sidebarActive, setSidebarActive] = useState(true);

    const handleSidebarCollapse = () => {
        setSidebarActive(!sidebarActive);
    }

    return (
        <>
            <div className="page-wraper">

                <EmpHeaderSection onClick={handleSidebarCollapse} sidebarActive={sidebarActive} />
                <EmpSidebarSection sidebarActive={sidebarActive} />

                <div id="content" className={sidebarActive ? "" : "active"}>
                    <div className="content-admin-main">
                        <EmployerRoutes />
                    </div>
                </div>

                <YesNoPopup id="delete-dash-profile" type={popupType.DELETE} msg={"Voulez-vous vraiment supprimer votre compte?"} />
                <YesNoPopup id="logout-dash-profile" type={popupType.LOGOUT} msg={"Voulez-vous vous déconnecter de votre compte?"} />
                
            </div>
        </>
    )
}

export default EmployerLayout;