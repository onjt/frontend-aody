import { useState } from "react";
import AdminSidebarSection from "../app/pannels/admin/common/admin-sidebar";
import AdminHeaderSection from "../app/pannels/admin/common/admin-header";
import AdminRoutes from "../routing/admin-routes";
import YesNoPopup from "../app/common/popups/popup-yes-no";
import { popupType } from "../globals/constants";

function AdminLayout() {

    const [sidebarActive, setSidebarActive] = useState(true);

    const handleSidebarCollapse = () => {
        setSidebarActive(!sidebarActive);
    }


    return (
        <>
            <div className="page-wraper">
                <AdminHeaderSection onClick={handleSidebarCollapse} sidebarActive={sidebarActive} />
                <AdminSidebarSection sidebarActive={sidebarActive} />

                <div id="content" className={sidebarActive ? "" : "active"}>
                    <div className="content-admin-main">
                        <AdminRoutes />
                    </div>
                </div>

            </div>
            <YesNoPopup id="logout-dash-profile" type={popupType.LOGOUT} msg={"Voulez-vous vous déconnecter de votre compte?"} />
        </>
    )
}
export default AdminLayout