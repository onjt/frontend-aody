import { Route, Routes } from "react-router-dom";
import AdminDashboardPage from "../app/pannels/admin/components/admin-dashboard";
import AdminManageStudent from "../app/pannels/admin/components/admin-manage-student";
import AdminManageCompany from "../app/pannels/admin/components/admin-manage-company";
import AdminManageJob from "../app/pannels/admin/components/admin-manage-job";
import CanChangePasswordPage from "../app/pannels/candidate/components/can-change-password";
import AdminSubscription from "../app/pannels/admin/components/admin-subscription";
import Error404Page from "../app/pannels/public-user/components/pages/error404";

function AdminRoutes() {
    return (
        <Routes>
            <Route path="/" element={<AdminDashboardPage />} />
            <Route path="/dashboard" element={<AdminDashboardPage />} />
            <Route path="/manage-student" element={<AdminManageStudent />} />
            <Route path="/manage-company" element={<AdminManageCompany />} />
            <Route path="/manage-jobs" element={<AdminManageJob />} />
            <Route path="/subscription" element={<AdminSubscription />} />
            <Route path="/manage-password" element={<CanChangePasswordPage />} />
            <Route path="*" element={<Error404Page />} />


        </Routes>
    )
}
export default AdminRoutes